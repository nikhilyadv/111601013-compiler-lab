structure Flow =
struct
datatype node = Node of {
                id: int,
                def: Temp.temp list,
                use: Temp.temp list,
                ismove: bool,            (* Tells whether each instruction is a move instruction, which could be deleted if the def and use are identical *)
                succ: node list ref,
                prev: node list ref,
                liveout: Temp.temp list ref}
type flowgraph = node list
end
