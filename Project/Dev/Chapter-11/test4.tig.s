.text
initArray:
	li $a2, 4
	mul $a0, $a0, $a2
	li $v0, 9
	syscall
	move $v1, $v0
	add $a0, $a0, $v0
	_initArray_0:
	sw $a1, ($v1)
	add $v1, $v1, 4
	bne $v1, $a0, _initArray_0
	jr $ra

allocRecord:
  li $a2, 4
  mul $a0, $a0, $a2
  li $v0, 9
  syscall
  jr $ra

printi:
    li $v0, 1
    syscall
    jr $ra

print:
    li $v0, 4
    syscall
    jr $ra

flush:
    jr $ra

strcmp:
    strcmptest:
    lb $a2 ($a0)
    lb $a3 ($a1)
    beq $a2, $zero, strcmpend
    beq $a3, $zero, strcmpend
    bgt $a2, $a3  strcmpgreat
    blt $a2, $a3  strcmpless
    add $a0, $a0, 1
    add $a1, $a1, 1
    j strcmptest
    strcmpgreat:
    li $v0, 1
    jr $ra
    strcmpless:
    li $v0, -1
    jr $ra
    strcmpend:
    bne $a2 $zero strcmpgreat
    bne $a3 $zero strcmpless
    li $v0, 0
    jr $ra

size:
    move $v0, $zero
    sizeloop:
    lb $a1 ($a0)
    beq $a1, $zero sizeexit
    add $v0, $v0, 1
    add $a0, $a0, 1
    j sizeloop
    sizeexit:
    jr $ra

ord:
    lb $a1,($a0)
    li $v0,-1
    beqz $a1,Lrunt5
    lb $v0,($a0)
    Lrunt5:
    jr $ra

getchar:
    li $v0, 9
    li $a0, 2
    syscall
    move $a0, $v0
    li $a1, 2
    li $v0, 8
    syscall
    move $v0, $a0
    jr $ra

chr:
    move $a1, $a0
    li $v0, 9
    li $a0, 2
    syscall
    sb $a1 ($v0)
    sb $zero 1($v0)
    jr $ra

exit:
    li $v0, 10
    syscall

substring:
    add $a1, $a0, $a1
    move $a3, $a1
    li $v0, 9
    add $a2, $a2, 1
    move $a0, $a2
    add $a0, $a0, 1
    syscall
    # got a new string in $v0
    add $a2,$a2,$a3
    add $a2,$a2,-1
    move $a0, $v0
    substringcopy:
    beq $a1 $a2 substringexit
    lb $a3 ($a1)
    sb $a3 ($a0)
    add $a1, $a1, 1
    add $a0, $a0, 1
    j substringcopy
    substringexit:
    sb $zero, ($a0)
    jr $ra

copy:
    copyloop:
    lb $a2, ($a1)
    beq $zero, $a2 copyexit
    sb $a2, ($a0)
    add $a0,$a0,1
    add $a1,$a1,1
    j copyloop
    copyexit:
    sb $zero, ($a0)
    move $v0, $a0
    jr $ra

concat:
    sw $a0, -4($sp)
    sw $a1, -8($sp)
    sw $ra, -12($sp)
    jal size
    li $a3, 1
    add $a3,$a3,$v0
    lw $a0, -8($sp)
    jal size
    add $a3, $a3, $v0
    move $a0, $a3
    li $v0, 9
    syscall
    move $a3, $v0
    move $a0, $v0
    lw   $a1, -4($sp)
    jal copy
    move $a0, $v0
    lw $a1, -8($sp)
    jal copy
    move $v0, $a3
    lw $ra, -12($sp)
    jr $ra
	
	.globl main
	.data
L48: .asciiz "
"
L40: .asciiz " ."
L39: .asciiz " O"

	.text
main:
	sw	$fp	0($sp)
	move	$fp	$sp
	addiu	$sp	$sp	-60
L73:

	sw $a0, 4($fp)

	move $t0, $ra

	sw $t0, -28($fp)

	sw $s0, -32($fp)

	move $s0, $s1

	sw $s0, -36($fp)

	move $s0, $s2

	sw $s0, -40($fp)

	move $s0, $s3

	sw $s0, -44($fp)

	move $s3, $s5

	move $s0, $s6

	move $s1, $s7

	li $s2, 8

	sw $s2, -4($fp)

	addi $s2, $fp, -8

	lw $s5, -4($fp)

	move $a0, $s5

	li $a1, 0

	jal initArray

	move $s5, $v0

	sw $s5, 0($s2)

	addi $s2, $fp, -12

	lw $s5, -4($fp)

	move $a0, $s5

	li $a1, 0

	jal initArray

	move $s5, $v0

	sw $s5, 0($s2)

	addi $s2, $fp, -16

	lw $s6, -4($fp)

	lw $s5, -4($fp)

	add $s5, $s6, $s5

	addi $s5, $s5, -1

	move $a0, $s5

	li $a1, 0

	jal initArray

	move $s5, $v0

	sw $s5, 0($s2)

	addi $s2, $fp, -20

	lw $s6, -4($fp)

	lw $s5, -4($fp)

	add $s5, $s6, $s5

	addi $s5, $s5, -1

	move $a0, $s5

	li $a1, 0

	jal initArray

	move $s5, $v0

	sw $s5, 0($s2)

	move $a0, $fp

	li $a1, 0

	jal try

	li $v0, 0

	move $s7, $s1

	move $s6, $s0

	move $s5, $s3

	lw $s0, -44($fp)

	move $s3, $s0

	lw $s0, -40($fp)

	move $s2, $s0

	lw $s0, -36($fp)

	move $s1, $s0

	lw $s0, -32($fp)

	lw $t0, -28($fp)

	move $ra, $t0

	j L72

L72:

	lw $t0, -24($fp)

	
	move	$sp	$fp
	lw	$fp	0($sp)
	jr	$ra

try:
	sw	$fp	0($sp)
	move	$fp	$sp
	addiu	$sp	$sp	-48
L75:

	sw $a0, 4($fp)

	move $t0, $a1

	sw $t0, -32($fp)

	move $t0, $ra

	sw $t0, -8($fp)

	sw $s0, -12($fp)

	move $s0, $s1

	sw $s0, -16($fp)

	move $s0, $s2

	sw $s0, -20($fp)

	move $s0, $s3

	sw $s0, -24($fp)

	move $s0, $s4

	sw $s0, -28($fp)

	move $s4, $s5

	move $s3, $s6

	move $s2, $s7

	lw $s0, 4($fp)

	lw $s1, -4($s0)

	lw $s0, -32($fp)

	beq $s0, $s1, L69
	bne $s0, $s1, L70

L70:

	li $s0, 0

	lw $s1, 4($fp)

	lw $s1, -4($s1)

	addi $s1, $s1, -1

L65:

	li $s6, 1

	ble $s0, $s1, L67
	bgt $s0, $s1, L68

L68:

	li $s6, 0

L67:

	li $s5, 0

	beq $s6, $s5, L53
	bne $s6, $s5, L66

L66:

	lw $s5, 4($fp)

	lw $s6, -8($s5)

	li $s5, 4

	mul $s5, $s0, $s5

	add $s5, $s6, $s5

	lw $s6, 0($s5)

	li $s5, 0

	beq $s6, $s5, L54
	bne $s6, $s5, L55

L55:

L59:

L63:

	addi $s0, $s0, 1

	j L65

L69:

	lw $s0, 4($fp)

	move $a0, $s0

	jal printboard

L71:

	li $v0, 0

	move $s7, $s2

	move $s6, $s3

	move $s5, $s4

	lw $s0, -28($fp)

	move $s4, $s0

	lw $s0, -24($fp)

	move $s3, $s0

	lw $s0, -20($fp)

	move $s2, $s0

	lw $s0, -16($fp)

	move $s1, $s0

	lw $s0, -12($fp)

	lw $t0, -8($fp)

	move $ra, $t0

	j L74

L54:

	lw $s5, 4($fp)

	lw $s7, -16($s5)

	lw $s5, -32($fp)

	add $s6, $s0, $s5

	li $s5, 4

	mul $s5, $s6, $s5

	add $s5, $s7, $s5

	lw $s6, 0($s5)

	li $s5, 0

	bne $s6, $s5, L59
	beq $s6, $s5, L58

L58:

	lw $s5, 4($fp)

	lw $s7, -20($s5)

	addi $s6, $s0, 7

	lw $s5, -32($fp)

	sub $s6, $s6, $s5

	li $s5, 4

	mul $s5, $s6, $s5

	add $s5, $s7, $s5

	lw $s6, 0($s5)

	li $s5, 0

	bne $s6, $s5, L63
	beq $s6, $s5, L62

L62:

	li $s7, 1

	lw $s5, 4($fp)

	lw $s6, -8($s5)

	li $s5, 4

	mul $s5, $s0, $s5

	add $s5, $s6, $s5

	sw $s7, 0($s5)

	li $t0, 1

	lw $s5, 4($fp)

	lw $s7, -16($s5)

	lw $s5, -32($fp)

	add $s6, $s0, $s5

	li $s5, 4

	mul $s5, $s6, $s5

	add $s5, $s7, $s5

	sw $t0, 0($s5)

	li $s5, 1

	lw $s6, 4($fp)

	lw $t0, -20($s6)

	addi $s7, $s0, 7

	lw $s6, -32($fp)

	sub $s7, $s7, $s6

	li $s6, 4

	mul $s6, $s7, $s6

	add $s6, $t0, $s6

	sw $s5, 0($s6)

	lw $s5, 4($fp)

	lw $s7, -12($s5)

	li $s6, 4

	lw $s5, -32($fp)

	mul $s5, $s5, $s6

	add $s5, $s7, $s5

	sw $s0, 0($s5)

	lw $s5, 4($fp)

	move $a0, $s5

	lw $s5, -32($fp)

	addi $s5, $s5, 1

	move $a1, $s5

	jal try

	li $s7, 0

	lw $s5, 4($fp)

	lw $s6, -8($s5)

	li $s5, 4

	mul $s5, $s0, $s5

	add $s5, $s6, $s5

	sw $s7, 0($s5)

	li $t0, 0

	lw $s5, 4($fp)

	lw $s7, -16($s5)

	lw $s5, -32($fp)

	add $s6, $s0, $s5

	li $s5, 4

	mul $s5, $s6, $s5

	add $s5, $s7, $s5

	sw $t0, 0($s5)

	li $s5, 0

	lw $s6, 4($fp)

	lw $t0, -20($s6)

	addi $s7, $s0, 7

	lw $s6, -32($fp)

	sub $s7, $s7, $s6

	li $s6, 4

	mul $s6, $s7, $s6

	add $s6, $t0, $s6

	sw $s5, 0($s6)

	j L63

L53:

	j L71

L74:

	lw $t0, -4($fp)

	
	move	$sp	$fp
	lw	$fp	0($sp)
	jr	$ra

printboard:
	sw	$fp	0($sp)
	move	$fp	$sp
	addiu	$sp	$sp	-48
L77:

	sw $a0, 4($fp)

	move $t0, $ra

	sw $t0, -8($fp)

	sw $s0, -12($fp)

	move $s0, $s1

	sw $s0, -16($fp)

	move $s0, $s2

	sw $s0, -20($fp)

	move $s0, $s3

	sw $s0, -24($fp)

	move $s0, $s4

	sw $s0, -28($fp)

	move $s0, $s5

	sw $s0, -32($fp)

	move $s5, $s6

	move $s4, $s7

	li $s0, 0

	lw $s1, 4($fp)

	lw $s1, -4($s1)

	addi $s1, $s1, -1

	move $s3, $s1

L49:

	li $s2, 1

	ble $s0, $s3, L51
	bgt $s0, $s3, L52

L52:

	li $s2, 0

L51:

	li $s1, 0

	beq $s2, $s1, L37
	bne $s2, $s1, L50

L50:

	li $s1, 0

	lw $s2, 4($fp)

	lw $s2, -4($s2)

	addi $s2, $s2, -1

L44:

	li $s7, 1

	ble $s1, $s2, L46
	bgt $s1, $s2, L47

L47:

	li $s7, 0

L46:

	li $s6, 0

	beq $s7, $s6, L38
	bne $s7, $s6, L45

L45:

	lw $s6, 4($fp)

	lw $s7, -12($s6)

	li $s6, 4

	mul $s6, $s0, $s6

	add $s6, $s7, $s6

	lw $s6, 0($s6)

	beq $s6, $s1, L41
	bne $s6, $s1, L42

L42:

	la $s6, L40

L43:

	move $a0, $s6

	jal print

	addi $s1, $s1, 1

	j L44

L41:

	la $s6, L39

	j L43

L38:

	la $s1, L48

	move $a0, $s1

	jal print

	addi $s0, $s0, 1

	j L49

L37:

	la $s0, L48

	move $a0, $s0

	jal print

	li $v0, 0

	move $s7, $s4

	move $s6, $s5

	lw $s0, -32($fp)

	move $s5, $s0

	lw $s0, -28($fp)

	move $s4, $s0

	lw $s0, -24($fp)

	move $s3, $s0

	lw $s0, -20($fp)

	move $s2, $s0

	lw $s0, -16($fp)

	move $s1, $s0

	lw $s0, -12($fp)

	lw $t0, -8($fp)

	move $ra, $t0

	j L76

L76:

	lw $t0, -4($fp)

	
	move	$sp	$fp
	lw	$fp	0($sp)
	jr	$ra

