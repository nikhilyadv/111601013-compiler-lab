type pos = int
type lexresult = Tokens.token

val lineNum = ErrorMsg.lineNum
val linePos = ErrorMsg.linePos
fun err(p1,p2) = ErrorMsg.error p1

val commentCounter = ref 0
val insideString = ref false
val startPosString = ref 0
val Buffer = ref ""

fun eof() = 
    let
        val pos = hd(!linePos) 
        val _ = if (!commentCounter > 0) then ErrorMsg.error pos "Unclosed comment at EOF"
                else if (!insideString = true) then ErrorMsg.error pos "Unclosed string at EOF"
                     else ()
    in 
        Tokens.EOF(pos,pos) 
    end

fun convertToAscii str =
    let
        val ss = String.substring(str, 1, 3)
        val cc = chr (valOf(Int.fromString(ss)))
    in
        Char.toString cc
    end

fun updateLine n = lineNum := !(lineNum) + n
fun updateCommentCount n = commentCounter := !(commentCounter) + n

%% 
%s COMMENT STRING;
digit = [0-9]+;

%%
<INITIAL>"if"           => (Tokens.IF(yypos, yypos+2));
<INITIAL>"then"         => (Tokens.THEN(yypos, yypos+4));
<INITIAL>"else"         => (Tokens.ELSE(yypos, yypos+4));
<INITIAL>"while"        => (Tokens.WHILE(yypos, yypos+5));
<INITIAL>"for"          => (Tokens.FOR(yypos, yypos+3));
<INITIAL>"to"           => (Tokens.TO(yypos, yypos+2));
<INITIAL>"do"           => (Tokens.DO(yypos, yypos+2));
<INITIAL>"let"          => (Tokens.LET(yypos, yypos+3));
<INITIAL>"in"           => (Tokens.IN(yypos, yypos+2));
<INITIAL>"end"          => (Tokens.END(yypos, yypos+3));
<INITIAL>"of"           => (Tokens.OF(yypos, yypos+2));
<INITIAL>"break"        => (Tokens.BREAK(yypos, yypos+5));
<INITIAL>"nil"          => (Tokens.NIL(yypos, yypos+3));
<INITIAL>"function"     => (Tokens.FUNCTION(yypos, yypos+8));
<INITIAL>"var"          => (Tokens.VAR(yypos, yypos+3));
<INITIAL>"type"         => (Tokens.TYPE(yypos, yypos+4));
<INITIAL>"array"        => (Tokens.ARRAY(yypos, yypos+5));

<INITIAL>","            => (Tokens.COMMA(yypos, yypos+1));
<INITIAL>":"            => (Tokens.COLON(yypos, yypos+1));
<INITIAL>";"            => (Tokens.SEMICOLON(yypos, yypos+1));
<INITIAL>")"            => (Tokens.RPAREN(yypos, yypos+1));
<INITIAL>"("            => (Tokens.LPAREN(yypos, yypos+1));
<INITIAL>"]"            => (Tokens.RBRACK(yypos, yypos+1));
<INITIAL>"["            => (Tokens.LBRACK(yypos, yypos+1));
<INITIAL>"}"            => (Tokens.RBRACE(yypos, yypos+1));
<INITIAL>"{"            => (Tokens.LBRACE(yypos, yypos+1));
<INITIAL>"."            => (Tokens.DOT(yypos, yypos+1));
<INITIAL>"+"            => (Tokens.PLUS(yypos, yypos+1));
<INITIAL>"-"            => (Tokens.MINUS(yypos, yypos+1));
<INITIAL>"*"            => (Tokens.TIMES(yypos, yypos+1));
<INITIAL>"/"            => (Tokens.DIVIDE(yypos, yypos+1));
<INITIAL>"="            => (Tokens.EQ(yypos, yypos+1));
<INITIAL>"<>"           => (Tokens.NEQ(yypos, yypos+2));
<INITIAL>">="           => (Tokens.GE(yypos, yypos+2));
<INITIAL>">"            => (Tokens.GT(yypos, yypos+1));
<INITIAL>"<="           => (Tokens.LE(yypos, yypos+2));
<INITIAL>"<"            => (Tokens.LT(yypos, yypos+1));
<INITIAL>"&"            => (Tokens.AND(yypos, yypos+1));
<INITIAL>"|"            => (Tokens.OR(yypos, yypos+1));
<INITIAL>":="           => (Tokens.ASSIGN(yypos, yypos+2));

<INITIAL>{digit}*       => (Tokens.INT(valOf(Int.fromString(yytext)), yypos, yypos+size yytext));
<INITIAL>[a-zA-Z][a-zA-Z0-9_]* => (Tokens.ID(yytext, yypos, yypos+size yytext));

<INITIAL>\n             => (updateLine 1; linePos := yypos :: !linePos; continue());
<INITIAL>[\ \t]+        => (continue());

<INITIAL>"/*"           => (commentCounter := 1; YYBEGIN COMMENT; continue());
<INITIAL>"*/"           => (ErrorMsg.error yypos ("Extra */"); continue());
<COMMENT>"/*"           => (commentCounter := (!commentCounter + 1); continue());
<COMMENT>"*/"           => (commentCounter := (!commentCounter - 1); if (!commentCounter = 0) then YYBEGIN INITIAL else (); continue());
<COMMENT>\n             => (updateLine 1; linePos := yypos :: !linePos; continue());
<COMMENT>[\ \t]+        => (continue());
<COMMENT>.              => (continue());

<INITIAL>\"             => (insideString := true; YYBEGIN STRING; startPosString := yypos; Buffer := ""; continue());
<STRING>\\\"            => (Buffer := !Buffer ^ "\""; continue());
<STRING>\\n             => (Buffer := !Buffer ^ "\n"; continue());
<STRING>\\t             => (Buffer := !Buffer ^ "\t"; continue());
<STRING>\\\\            => (Buffer := !Buffer ^ "\\"; continue());
<STRING>[ -!#-\[\]-~\*] => (Buffer := !Buffer ^ yytext; continue());
<STRING>\\{digit}{digit}{digit} => (Buffer := !Buffer ^ convertToAscii(yytext); continue());
<STRING>\\[\n\t \f]+\\  => (continue());
<STRING>\"              => (insideString := false; YYBEGIN INITIAL; Tokens.STRING(!Buffer, !startPosString, yypos));
<STRING>\n              => (ErrorMsg.error yypos ("Error due to new line character"); continue());
<STRING>.               => (ErrorMsg.error yypos ("Error due to String - " ^ yytext); continue());


<INITIAL>.              => (ErrorMsg.error yypos ("Error due to - " ^ yytext); continue());
