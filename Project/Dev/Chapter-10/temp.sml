(* The compiler's semantic analysis phase will want to choose registers for 
parameters and local variables, and choose machine-code addresses for 
procedure bodies. But it is too early to determine exactly which registers are 
available, or exactly where a procedure body will be located. We use the word
temporary to mean a value that is temporarily held in a register, and the word
label to mean some machine-language location whose exact address is yet to
be determined -just like a label in assembly language.
Temps are abstract names for local variables; labels are abstract names
for static memory addresses. The Temp module manages these two distinct
sets of names. *)
signature TEMP = 
sig
  eqtype temp
  val newtemp : unit -> temp
  structure Table : TABLE sharing type Table.key = temp
  val makestring: temp -> string
  type label = Symbol.symbol
  val newlabel : unit -> label
  val namedlabel : string -> label
end

structure Temp : TEMP =
struct
    type temp = int
    val temps = ref 100
    (* Temp. newtemp () returns a new temporary from an infinite set of temps. *)

    fun newtemp() = let val t = !temps in temps := t+1; t end

    structure Table = IntMapTable(type key = int
				  fun getInt n = n)

    fun makestring t = "t" ^ Int.toString t

  type label = Symbol.symbol

local structure F = Format
      fun postinc x = let val i = !x in x := i+1; i end
      val labs = ref 0
  in
    (* Temp.newlabel () returns a new label from an infinite set of labels. And Temp.namedlabel (string) returns a new label whose assembly-language name is string. *)
    fun newlabel() = Symbol.symbol(F.format "L%d" [F.INT(postinc labs)])
    val namedlabel = Symbol.symbol
  end
end
