signature FRAME =
sig
  type frame
  type access

  val newFrame : {name: Temp.label, formals: bool list} -> frame
  val name: frame -> Temp.label
  val formals: frame -> access list
  val allocLocal : frame -> bool -> access
  val string : Tree.label * string -> string

  val FP: Temp.temp
  val SP: Temp.temp
  val RV: Temp.temp
  val RA: Temp.temp
  val ZERO: Temp.temp
  val wordSize: int
  val exp: access -> Tree.exp -> Tree.exp

  type register = string
  val registers : register list
  val argregs : Temp.temp list
  val callersaves : Temp.temp list
  val calleesaves : Temp.temp list

  val procEntryExit1 : frame * Tree.stm -> Tree.stm
  val procEntryExit2 : frame * Assem.instr list -> Assem.instr list
  val procEntryExit3 : frame * Assem.instr list ->
                       {prolog:string,body:Assem.instr list,epilog:string}

  val makestring : Temp.temp -> register
  val getTempName: Temp.temp -> string

  val tempMap : register Temp.Table.table
  val temp_name : Temp.temp -> string

  datatype frag = PROC of {body: Tree.stm, frame: frame}
                | STRING of Temp.label * string

  val externalCall : string * Tree.exp list -> Tree.exp
end

(* a data structure holding:
    • the locations of all the formals,
    • instructions required to implement the "view shift,"
    • the number of locals allocated so far,
    • and the label at which the function's machine code is to begin 
  Frame should not know anything about static links.*)
structure MipsFrame : FRAME = struct

  structure T = Tree
  structure S = Symbol
  structure TP = Temp
  structure A = Assem

  (* The access type describes formals and locals that may be in the frame or
    in registers. *)
  datatype access = InFrame of int              (*Memory location at offset x from frame pointer*)
                  | InReg of Temp.temp          (*Register temp*)
  (* inFrame (X) indicates a memory location at offset X from the frame pointer;
    InReg(t84) indicates that it will be held in "register" t84 *)
  type register = string
  val wordSize = 4
  type frame = {name: TP.label, formals: access list, locals: int ref, instrs: T.stm list }

  (* The semantic analysis phase calls upon Translate.newLevel in 
    processing a function header. Later it calls other interface fields of Translate
    to translate the body of the Tiger function; this has the side effect of 
    remembering string fragments for any string literals encountered Finally the semantic analyzer calls procEntryExit, which has
    the side effect of remembering a proc fragment.*)
  datatype frag = PROC of {body:T.stm, frame:frame}
	              | STRING of TP.label * string

  val v0 = TP.newtemp()
  val v1 = TP.newtemp()

  val a0 = TP.newtemp()
  val a1 = TP.newtemp()
  val a2 = TP.newtemp()
  val a3 = TP.newtemp()

  (* CallerSaved Register *)

  val t0 = TP.newtemp()
  val t1 = TP.newtemp()
  val t2 = TP.newtemp()
  val t3 = TP.newtemp()
  val t4 = TP.newtemp()
  val t5 = TP.newtemp()
  val t6 = TP.newtemp()
  val t7 = TP.newtemp()

  (* CalleeSaved Register *)

  val s0 = TP.newtemp()
  val s1 = TP.newtemp()
  val s2 = TP.newtemp()
  val s3 = TP.newtemp()
  val s4 = TP.newtemp()
  val s5 = TP.newtemp()
  val s6 = TP.newtemp()
  val s7 = TP.newtemp()

  val t8 = TP.newtemp()
  val t9 = TP.newtemp()

  val ZERO = TP.newtemp()
  val GP = TP.newtemp()
  val FP = TP.newtemp()
  val SP = TP.newtemp()
  val RA = TP.newtemp()
  val RV = TP.newtemp()

  val specialargs = [RV,FP,SP,RA]

  val argregs = [a0,a1,a2,a3]

  val calleesaves = [s0,s1,s2,s3,s4,s5,s6,s7]

  val callersaves = [t0,t1,t2,t3,t4,t5,t6,t7]

  val reglist =
      [("$a0",a0),("$a1",a1),("$a2",a2),("$a3",a3),
      ("$t0",t0),("$t1",t1),("$t2",t2),("$t3",t3),
      ("$t4",t4),("$t5",t5),("$t6",t6),("$t7",t7),
      ("$s0",s0),("$s1",s1),("$s2",s2),("$s3",s3),
      ("$s4",s4),("$s5",s5),("$s6",s6),("$s7",s7),
      ("$fp",FP),("$v0",RV),("$sp",SP),("$ra",RA)]

val tempMap = foldl
  (fn ((k,v),tb) => TP.Table.enter(tb,v,k)) TP.Table.empty reglist

fun temp_name t =
    case TP.Table.look(tempMap,t) of
	      SOME(r) => r
      | NONE => Temp.makestring t

fun makestring t = (* replacement for temp.makestring *)
    case Temp.Table.look(tempMap, t) of
          SOME(r) => r
        | NONE => Temp.makestring t

(* a list of all register name, which can be used for coloring *)
val registers = map (fn (x) => case TP.Table.look(tempMap,x) of
                        SOME(r) => r) (argregs @ calleesaves @ callersaves @ specialargs)

fun string (label, str) : string =
    S.name label ^ ": .asciiz \"" ^ str ^ "\"\n"
(* The function Frame. exp is used by Translate to turn a Frame. access
  into the Tree expression. The Tree. exp argument to Frame. exp is the
  address of the stack frame that the access lives in. *)
fun exp (InFrame(k))= (fn (temp) => T.MEM(T.BINOP(T.PLUS,temp,T.CONST k)))
  | exp (InReg(temp)) = (fn (_) => T.TEMP temp)

exception TooManyArgs of string

(* The type frame holds information about formal parameters and local 
variables allocated in this frame. To make a new frame for a function f with k
formal parameters, call newFrame{name=f, formals=l}, where l is a list
of k booleans: true for each parameter that escapes and false for each
parameter that does not. *)
(* For each formal parameter, newFrame must calculate two things:
      • How the parameter will be seen from inside the function (in a register, or in a
frame location);
      • What instructions must be produced to implement the "view shift." *)
fun newFrame {name: TP.label, formals: bool list} =
    let
      val n = List.length formals
      fun iter(nil,_) = nil
        | iter(b::rest,offset) =
          if b then InFrame(offset)::iter(rest,offset+wordSize)
          else InReg(TP.newtemp())::iter(rest,offset)
      val accs : access list = iter(formals,wordSize) (* save old FP at offset 0 *)
      fun view_shift(acc,r) = T.MOVE(exp acc (T.TEMP FP), T.TEMP r)
      val shift_instrs = ListPair.map view_shift (accs,argregs)
    in if n <= List.length argregs then
         {name=name,formals=accs,locals=ref 0,instrs=shift_instrs}
       else raise TooManyArgs("Too many arguments: " ^ Int.toString(~n))
    end

  fun name ({name,...}: frame) = name

  (* The Frame. formals interface function extracts a list of it "accesses" 
  denoting the locations where the formal parameters will be kept at run time, as
  seen from inside the callee. *)
  fun formals ({formals,...}: frame): access list = formals

  (* allocate a local variable either on frame or in register *)
  (* This returns an InFrame access with an offset from the frame pointer. *)
  fun allocLocal ({locals,...}: frame) escape =
      if (escape) then
        let val ret = InFrame((!locals+1)*(~wordSize)) in
          locals := !locals + 1; ret end
      else InReg(TP.newtemp())


  fun externalCall (s,args) = T.CALL(T.NAME(TP.namedlabel s), args)

  fun seq nil = T.EXP(T.CONST 0)
    | seq [st] = st
    | seq (st :: rest) = T.SEQ(st,seq(rest))

  fun getTempName(temp) =
    let 
      val name = TP.Table.look(tempMap, temp)
    in
      case name of NONE => TP.makestring(temp)
                 | SOME(regstr) => regstr
    end

  (* for each incoming register parameter, move it to the place
  * from which it is seem from within the function. This could be
  * a frame location (for escaping parameters) or a fresh temporary.*)
  (* Finally the semantic analyzer calls procEntryExit, which has
  the side effect of remembering a proc fragment.
  All the remembered fragments go into a frag list ref local to 
  Translate; then getResult can be used to extract the fragment list. *)
  (* This is dummy implementation *)
  fun procEntryExit1 (frame,body) : T.stm =
    let
      val args = #instrs frame
      val pairs =  map (fn r => (allocLocal frame false,r)) (RA::calleesaves)
      val saves = map (fn (a,r) => T.MOVE(exp a (T.TEMP FP),T.TEMP r)) pairs
      val restores = map (fn (a,r) => T.MOVE(T.TEMP r,exp a (T.TEMP FP)))
                        (List.rev pairs)
    in seq(args @saves @ [body] @ restores) end

  fun procEntryExit2 (frame,body) =
    body @
    [A.OPER{assem="",
            src=[ZERO,RA,SP]@calleesaves,
            dst=[],jump=SOME[]}]

  fun procEntryExit3 ({name,formals,locals,instrs},body) =
    let val offset = (!locals + (List.length argregs))*wordSize in
      {prolog=S.name name ^ ":\n" ^
              "\tsw\t$fp\t0($sp)\n" ^ (* save old FP *)
              "\tmove\t$fp\t$sp\n" ^ (* make SP to be new FP *)
              "\taddiu\t$sp\t$sp\t-" ^ Int.toString(offset) ^ "\n" (* make new SP *),
       body=body,
       epilog="\tmove\t$sp\t$fp\n" ^ (* restore old SP *)
              "\tlw\t$fp\t0($sp)\n" ^ (* restore old FP *)
              "\tjr\t$ra\n\n" (* jump to return address *)
      }
      end
end