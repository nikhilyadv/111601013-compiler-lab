structure A = Absyn

fun temp () = ()

%%
%term
    EOF 
  | ID of string
  | INT of int | STRING of string 
  | COMMA | COLON | SEMICOLON | LPAREN | RPAREN | LBRACK | RBRACK 
  | LBRACE | RBRACE | DOT 
  | PLUS | MINUS | TIMES | DIVIDE | EQ | NEQ | LT | LE | GT | GE
  | AND | OR | ASSIGN
  | ARRAY | IF | THEN | ELSE | WHILE | FOR | TO | DO | LET | IN | END | OF 
  | BREAK | NIL
  | FUNCTION | VAR | TYPE | UMINUS

%nonterm  exp of A.exp
        | program of A.exp
        | exps of (A.exp * pos) list
        | decs of A.dec list
        | dec of A.dec 
        | vardec of A.dec       
        | tydec of A.dec
        | fundec of A.dec
        | paramlist of A.exp list
        | paramlist_ of A.exp list
        | recordbody of (A.symbol * A.exp * pos) list
        | ty of A.ty
        | tyfields of A.field list
        | tyfields_ of A.field list
        | lvalue of A.var
        | letbody of A.exp


%pos int
%verbose
%start program
%eop EOF
%noshift EOF

%name Tiger

%keyword WHILE FOR TO BREAK LET IN END FUNCTION VAR TYPE ARRAY IF THEN ELSE 
	DO OF NIL

%nonassoc ASSIGN
%nonassoc ID
%nonassoc WHILE DO
%right THEN
%right ELSE
%nonassoc LBRACK RBRACK OF
%left OR
%left AND
%nonassoc EQ NEQ LT LE GT GE
%left PLUS MINUS
%left TIMES DIVIDE
%left UMINUS

%value ID ("bogus")
%value INT (1)
%value STRING ("")

%%

program	: exp				                            (exp)

exp: NIL 			                                  (A.NilExp)
    |lvalue                                     (A.VarExp(lvalue))
    |INT                                        (A.IntExp(INT))
    |STRING                                     (A.StringExp(STRING, STRINGleft))
    |LPAREN exp SEMICOLON exp exps RPAREN       (A.SeqExp((exp1, exp1left)::(exp2, exp2left)::exps))    
    |LPAREN RPAREN                              (A.SeqExp([]))
    |LPAREN exp RPAREN                          (exp)
    |LET decs IN letbody END                    (A.LetExp({decs=decs, body=letbody, pos=LETleft}))
    |exp PLUS exp                               (A.OpExp({left=exp1, oper=A.PlusOp, right=exp2, pos=exp1left}))
    |exp MINUS exp                              (A.OpExp({left=exp1, oper=A.MinusOp, right=exp2, pos=exp1left}))
    |exp TIMES exp                              (A.OpExp({left=exp1, oper=A.TimesOp, right=exp2, pos=exp1left}))
    |exp DIVIDE exp                             (A.OpExp({left=exp1, oper=A.DivideOp, right=exp2, pos=exp1left}))
    |exp EQ exp                                 (A.OpExp({left=exp1, oper=A.EqOp, right=exp2, pos=exp1left}))
    |exp NEQ exp                                (A.OpExp({left=exp1, oper=A.NeqOp, right=exp2, pos=exp1left}))
    |exp LT exp                                 (A.OpExp({left=exp1, oper=A.LtOp, right=exp2, pos=exp1left}))
    |exp LE exp                                 (A.OpExp({left=exp1, oper=A.LeOp, right=exp2, pos=exp1left}))
    |exp GT exp                                 (A.OpExp({left=exp1, oper=A.GtOp, right=exp2, pos=exp1left}))
    |exp GE exp                                 (A.OpExp({left=exp1, oper=A.GeOp, right=exp2, pos=exp1left}))
    |exp AND exp                                (A.IfExp({test=exp1, then'=exp2, else'=SOME(A.IntExp(0)), pos=exp1left}))
    |exp OR exp                                 (A.IfExp({test=exp1, then'=A.IntExp(1), else'=SOME(exp2), pos=exp1left}))
    |MINUS exp %prec UMINUS                     (A.OpExp({left=A.IntExp(0), oper=A.MinusOp, right=exp, pos=MINUSleft}))
    |lvalue ASSIGN exp                          (A.AssignExp({var=lvalue, exp=exp, pos=lvalueleft}))
    |IF exp THEN exp ELSE exp                   (A.IfExp({test=exp1, then'=exp2, else'=SOME exp3, pos=IFleft}))    
    |IF exp THEN exp                            (A.IfExp({test=exp1, then'=exp2, else'=NONE, pos=IFleft}))
    |WHILE exp DO exp                           (A.WhileExp({test=exp1, body=exp2, pos=WHILEleft}))
    |FOR ID ASSIGN exp TO exp DO exp            (A.ForExp({var=Symbol.symbol ID, escape=(ref true), lo=exp1, hi=exp2, body=exp3, pos=FORleft}))
    |BREAK                                      (A.BreakExp(BREAKleft))
    |ID LPAREN paramlist RPAREN                 (A.CallExp({func=Symbol.symbol(ID), args=paramlist, pos=IDleft}))
    |ID LBRACK exp RBRACK OF exp                (A.ArrayExp({typ=Symbol.symbol ID, size=exp1, init=exp2, pos=IDleft}))
    |ID LBRACE recordbody RBRACE                (A.RecordExp({fields=recordbody, typ=Symbol.symbol ID, pos=IDleft}))
  
exps: SEMICOLON exp exps                        ((exp, expleft)::exps)
     |                                          ([])

recordbody: ID EQ exp                           ([(Symbol.symbol ID, exp, defaultPos)])
           |ID EQ exp COMMA recordbody          ((Symbol.symbol ID, exp, defaultPos) :: recordbody)
           |                                    ([])

dec: vardec                                     (vardec)
    |tydec                                      (tydec)
    |fundec                                     (fundec)

decs: dec decs                                  (dec :: decs)
     |                                          ([])

vardec: VAR ID ASSIGN exp                       (A.VarDec({name=Symbol.symbol ID, escape=(ref true), typ=NONE, init=exp, pos=VARleft}))
       |VAR ID COLON ID ASSIGN exp              (A.VarDec({name=Symbol.symbol ID1, escape=(ref true), typ=SOME (Symbol.symbol ID2, ID2left), init=exp, pos=VARleft}))

tydec: TYPE ID EQ ty                            (A.TypeDec([{name=Symbol.symbol ID, ty=ty, pos=TYPEleft}]))

ty: ID                                          (A.NameTy(Symbol.symbol ID, IDleft))
   |LBRACE tyfields RBRACE                      (A.RecordTy(tyfields))
   |ARRAY OF ID                                 (A.ArrayTy(Symbol.symbol ID, ARRAYleft))

tyfields: ID COLON ID tyfields_                 ({name=Symbol.symbol ID1, escape=(ref true), typ=Symbol.symbol ID2, pos=ID1left}::tyfields_) 
         |                                      ([])

tyfields_: COMMA ID COLON ID tyfields_          ({name=Symbol.symbol ID1, escape=(ref true), typ=Symbol.symbol ID2, pos=ID1left}::tyfields_) 
          |                                     ([])

fundec: FUNCTION ID LPAREN tyfields RPAREN EQ exp           (A.FunctionDec ([{name=Symbol.symbol ID, params=tyfields, result=NONE, body=exp, pos=FUNCTIONleft}]))
       |FUNCTION ID LPAREN tyfields RPAREN COLON ID EQ exp  (A.FunctionDec ([{name=Symbol.symbol ID1, params=tyfields, result=SOME(Symbol.symbol ID2, ID2left), body=exp, pos=FUNCTIONleft}]))

letbody: exp exps                               (A.SeqExp((exp,expleft)::exps))
        |                                       (A.NilExp)

lvalue: ID                                      (A.SimpleVar(Symbol.symbol ID, IDleft))
       |lvalue DOT ID                           (A.FieldVar(lvalue, Symbol.symbol ID, lvalueleft))
       |ID LBRACK exp RBRACK                    (A.SubscriptVar(A.SimpleVar(Symbol.symbol ID, IDleft), exp, IDleft))
       |lvalue LBRACK exp RBRACK                (A.SubscriptVar(lvalue, exp, lvalueleft))

paramlist: exp paramlist_                       (exp::paramlist_)
          |                                     ([])

paramlist_: COMMA exp paramlist_                (exp::paramlist_)
           |                                    ([])