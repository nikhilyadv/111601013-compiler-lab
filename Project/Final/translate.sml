(* In the semantic analysis phase of the Tiger compiler, transDec creates
a new "nesting level" for each function by calling Translate.newLevel.
That function in turn calls Frame. newFrame to make a new frame. Semant
keeps this level in its FunEntry data structure for the function, so that
when it comes across a function call it can pass the called function's level
back to Translate. The FunEntry also needs the label of the function's
machine-code entry point *)
structure A = Absyn
structure T = Tree
structure Frame : FRAME = MipsFrame
structure F = Frame

signature TRANSLATE = sig
  type level
  type access (*not same as Frame.access*)

  type exp
  type frag
  val outermost : level
  val newLevel : {parent: level, name: Temp.label, formals: bool list} -> level

  val simpleVar : access * level -> exp
  val subscriptVar : exp * exp -> exp
  val fieldVar : exp * Symbol.symbol * Symbol.symbol list -> exp
  val intlit : int -> exp
  val strlit : string -> exp
  val relop : Absyn.oper * exp * exp -> exp
  val binop : Absyn.oper * exp * exp -> exp
  val ifelse : exp * exp * exp option -> exp
  val record : exp list -> exp
  val array : exp * exp -> exp
  val loop : exp * exp * Temp.label -> exp
  val break : Temp.label -> exp
  val call : level * level * Temp.label * exp list * bool -> exp
  val assign : exp * exp -> exp
  val sequence : exp list -> exp
  val nilexp : exp
  val letexp : exp list * exp -> exp

  val getResult : unit -> frag list
  val reset : unit -> unit

  structure Frame : FRAME
  val procEntryExit : level * exp -> unit
  val errexp : exp

  val formals : level -> access list
  val allocLocal : level -> bool -> access
end

structure Translate = struct 

  datatype level = Top
                  |Lev of {parent: level, frame: Frame.frame} * unit ref

  (* The abstract data type Translate. access can be implemented as a pair
    consisting of the variable's level and its Frame. access 
    so that Translate.allocLocal calls Frame.allocLocal, and also 
    remembers what level the variable lives in. The level information will be 
    necessary later for calculating static links, when the variable is accessed from a
    (possibly) different level.*)
  type access = level * Frame.access

  type frag = F.frag

  val outermost = Top

  (* Ex stands for an "expression," represented as a Tree. exp.
     Nx stands for "no result," represented as a Tree statement.
     Cx stands for "conditional," represented as a function from label-pair to statement. If you pass it a true-destination and a false-destination, it will make a statement that evaluates some conditionals and then jumps to one of the destinations (the statement will never "fall through").*)
  datatype exp = Ex of T.exp
                |Nx of T.stm
                |Cx of Temp.label * Temp.label -> T.stm
  
  val fragments : frag list ref = ref nil

  fun reset () = fragments := nil
  
  fun getResult () = !fragments

  val errexp = Ex(T.CONST 0)

  (* The static link
    is passed to a function in a register and stored into the frame. Since the static
    link behaves so much like a formal parameter, we will treat it as one *)
  fun newLevel {parent,name,formals} = Lev ({parent=parent,frame=Frame.newFrame{name=name,formals=true::formals}}, ref ())
  
  (* to return formals associated with the frame in current level excluding the static link *)
  fun formals level = case level of 
                        Top => nil
                      | Lev({parent=parent,frame=frame},_) => let val formals = tl (Frame.formals frame) in (map (fn (x) => (level,x)) formals) end
  
  (* When Semant processes a local variable declaration at level lev, it calls
    Translate.allocLocal(lev)(esc) to create the variable in this level; the argument
    esc specifies whether the variable escapes. The result is a Translate. 
    access, which is an abstract data type (not the same as Frame. access, since
    it must know about static links). Later, when the variable is used in an 
    expression, Semant can hand this access back to Translate in order to generate 
    the machine code to access the variable. Meanwhile, Semant records the 
    access in each VarEntry in the value-environment.*)
  fun allocLocal level escape = case level of
                                  Lev({parent=_,frame=frame},_) => (level,Frame.allocLocal frame escape)
  
  fun seq stmlist = case stmlist of 
                      [s] => s
                    | [s1,s2] => T.SEQ(s1,s2)
                    | stm :: rest => T.SEQ(stm,seq(rest))

  (* Each of these behaves as if it were simply stripping off the corresponding constructor (Ex, Nx, or Cx) *)

  (* unEx : exp -> Tree.exp *)
  (* To convert a "conditional" into a "value expression," we invent a new temporary r and new labels t and f. Then we make a Tree.stm that moves the value l into r, and a conditional jump gens tm(r, f) that implements the conditional. If the condition is false, then 0 is moved into r; if true, then execution proceeds at t and the second move is skipped. The result of the whole thing is just the temporary r containing zero or one. *)
  fun unEx (Ex e) = e
    | unEx (Cx genstm) =
      let val r = Temp.newtemp()
          val t = Temp.newlabel()
          val f = Temp.newlabel()
      in T.ESEQ(seq[T.MOVE(T.TEMP r, T.CONST 1),
                    genstm(t,f),
                    T.LABEL f,
                    T.MOVE(T.TEMP r, T.CONST 0),
                    T.LABEL t],
                T.TEMP r)
      end
    | unEx (Nx s) = T.ESEQ(s, T.CONST 0)

  (* unNx : exp -> Tree.stm *)
  fun unNx (Ex e) = T.EXP e                                           (* Simple *)
    | unNx (Nx s) = s
    | unNx (Cx genstm) =
      let val t = Temp.newlabel() in genstm(t,t); T.LABEL t end

  (* unCx : exp -> (Temp.label*Temp.label->Tree.stm *)
  fun unCx (Cx genstm) = genstm
    | unCx (Ex (T.CONST 0)) = (fn (t,f) => T.JUMP(T.NAME f,[f]))      (* For simple and efficient translation *)
    | unCx (Ex (T.CONST 1)) = (fn (t,f) => T.JUMP(T.NAME t,[t]))
    | unCx (Ex e) = (fn (t,f) => T.CJUMP(T.EQ, e, T.CONST 0, f, t))
    | unCx (Nx _) = raise ErrorMsg.Error                              (* This will not occur in a well typed tiger program *)

  val nilexp = Ex(T.CONST(0))

  fun intlit (n:int) : exp = Ex(T.CONST(n))

  fun strlit (s:string) : exp = let val t = List.find (fn (x) => case x of 
                                                                   F.PROC _ => false
                                                                 | F.STRING(_,s') => s = s') (!fragments)
                                in case t of
                                     NONE => let val l = Temp.newlabel() in (fragments := F.STRING(l,s) :: !fragments; Ex(T.NAME(l))) end
                                   | SOME(F.STRING(lab,_)) => Ex(T.NAME(lab))
                                end
  
  fun binop (oper,e1,e2) : exp =
      let
        val left = unEx(e1)
        val right = unEx(e2)
        val treeop =
            case oper of
              A.PlusOp => T.PLUS
            | A.MinusOp => T.MINUS
            | A.TimesOp => T.MUL
            | A.DivideOp => T.DIV
      in Ex(T.BINOP(treeop,left,right))
      end

  fun relop (oper,e1,e2) : exp =
      let
        val left = unEx(e1)
        val right = unEx(e2)
        val treeop =
            case oper of
              A.EqOp => T.EQ
            | A.NeqOp => T.NE
            | A.LtOp => T.LT
            | A.LeOp => T.LE
            | A.GtOp => T.GT
            | A.GeOp => T.GE
      in Cx((fn (t,f) => T.CJUMP(treeop,left,right,t,f))) end

  fun memplus (e1:T.exp,e2:T.exp) = T.MEM(T.BINOP(T.PLUS,e1,e2))

  (* fetch static links between the level of use (the level passed to simpleVar) and the level of definition (the level within the variable's access) *)
  fun simpleVar (access,level): exp =
      let val (Lev(_,defref),defaccess) = access
          fun iter (curlevel, acc) =
              let val Lev({parent,frame},curref) = curlevel
              in if (defref = curref) then
                  Frame.exp(defaccess)(acc)
                else let val staticlink = hd(Frame.formals frame)
                      in iter(parent,Frame.exp(staticlink)(acc))
                      end
              end
      in Ex(iter(level,T.TEMP(Frame.FP))) 
      end

  fun subscriptVar (base,offset): exp = Ex(memplus(unEx(base),T.BINOP(T.MUL,unEx(offset),T.CONST(Frame.wordSize))))

  fun fieldVar (base,id,flist) : exp =
      (* pre-condition: id in flist *)
      let fun findindex (index,elem,list) =
              if elem = hd(list) then index
              else findindex(index+1,elem,tl(list)) in
        Ex(memplus(unEx(base),
                  T.BINOP(T.MUL,T.CONST(findindex(0,id,flist)),
                                      T.CONST(Frame.wordSize))))
      end

  (* The elseexp could be NONE, in which case the
  * result must be unit, and thus we return CONST(0) *)
  fun ifelse (testexp,thenexp,elseexp: exp option) : exp =
      let
        val r = Temp.newtemp() (* hold result *)
        val t = Temp.newlabel()
        val f = Temp.newlabel()
        val finish = Temp.newlabel()
        val testfun  = unCx(testexp) in
        case thenexp of
            Ex(e) =>
            (case elseexp of
                SOME(e') => (* Nx possible? *)
                Ex(T.ESEQ(seq[testfun(t,f),
                              T.LABEL t, T.MOVE(T.TEMP r, e),
                              T.JUMP (T.NAME finish, [finish]),
                              T.LABEL f, T.MOVE(T.TEMP r, unEx(e')),
                              T.JUMP (T.NAME finish, [finish]),
                              T.LABEL finish],
                          T.TEMP r)))
          | Nx(thenstm) =>
            (case elseexp of
                NONE =>
                Nx(seq[testfun(t,f),
                        T.LABEL t, thenstm,
                        T.LABEL f])
              | SOME(st) => (* must be a statement *)
                Nx(seq[testfun(t,f),
                        T.LABEL t, thenstm,
                        T.JUMP (T.NAME finish, [finish]),
                        T.LABEL f, unNx(st),
                        T.JUMP (T.NAME finish, [finish]),
                        T.LABEL finish]))
          | Cx(cf) => (* TODO: fix this *)
            let
              val z = Temp.newlabel ()
            in
              case elseexp of
                  SOME(e) =>
                  Cx(fn(t',f') =>
                        seq[testfun(t,f),
                            T.LABEL t, cf(t',f'),
                            T.LABEL f, (unCx e)(t',f')])
            end
      end

  (* creating a record (see page 164) *)
  fun record (fields) : exp =
      let
        val r = Temp.newtemp()
        val init =
            T.MOVE(
            T.TEMP r,
            F.externalCall(
            "allocRecord", [T.CONST(length(fields)*F.wordSize)]))

        fun loop (fields,index) =
            case fields of
              nil => nil
            | e :: rest =>
              T.MOVE(
              memplus(T.TEMP r, T.CONST(index*F.wordSize)),
              unEx(e)) :: loop(rest,index+1)
      in Ex(T.ESEQ(seq(init::loop(fields,0)),T.TEMP r))
      end


  fun array (size, init) : exp =
      Ex(F.externalCall("initArray", [unEx(size),unEx(init)]))

  fun assign (left,right) : exp =
      Nx(T.MOVE(unEx(left),unEx(right)))

  fun loop (test, body, done_label) =
      let val test_label = Temp.newlabel()
          val body_label = Temp.newlabel() in
        Nx(seq[
        T.LABEL test_label,
        T.CJUMP(T.EQ,unEx(test),T.CONST 0, done_label, body_label),
        T.LABEL body_label,
        unNx(body),
        T.JUMP(T.NAME test_label, [test_label]),
        T.LABEL done_label])
      end

  fun break (label) : exp = Nx(T.JUMP(T.NAME label, [label]))

  fun call (_,Lev({parent=Top,...},_),label,exps,isprocedure) : exp = (* external call *)
      if isprocedure
      then Nx(T.EXP(F.externalCall(Symbol.name label,map unEx exps)))
      else Ex(F.externalCall(Symbol.name label,map unEx exps))

    | call (uselevel,deflevel,label,exps,isprocedure) : exp =
      (* find the difference
      * of static nesting depth between uselevel and deflevel *)
      let
        fun depth level =
              case level of
                Top => 0
              | Lev({parent,...},_) => 1 + depth(parent)
        val diff = depth uselevel - depth deflevel + 1
        fun iter (d,curlevel) =
            if d = 0 then T.TEMP Frame.FP
            else
              let val Lev({parent,frame},_) = curlevel in
                Frame.exp(hd(Frame.formals frame))(iter(d-1,parent))
              end
        val call = T.CALL(T.NAME label,(iter(diff,uselevel)) :: (map unEx exps))
      in if isprocedure
        then Nx(T.EXP(call)) else Ex(call)
      end

  (* result is the last exp. note that the last sequence
  * might be a statement, which makes the whole sequence statement. *)
  fun sequence (exps: exp list) =
      let val len = length exps in
        if len = 0 then Nx(T.EXP(T.CONST 0)) (* () is a statement *)
        else if len = 1 then hd(exps)
        else
          let val first = seq(map unNx (List.take(exps,length(exps)-1)))
              val last = List.last(exps) in
            case last of
              Nx(s) => Nx(T.SEQ(first,s))
            | _ => Ex(T.ESEQ(first,unEx(last)))
          end
      end

  fun letexp (decs,body) =
      let val len = List.length decs in
        if len = 0 then body
        else if len = 1 then Ex(T.ESEQ(unNx(hd(decs)),unEx(body)))
        else let val s = map unNx decs in Ex(T.ESEQ(seq s,unEx(body))) end
      end


  fun procEntryExit (Lev({frame,...},_),body) =
      let val body' =
              Frame.procEntryExit1(frame,T.MOVE(T.TEMP Frame.RV,unEx(body)))
      in fragments := Frame.PROC{frame=frame,body=body'} :: !fragments
      end
end