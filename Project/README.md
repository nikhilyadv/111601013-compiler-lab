Nikhil Kumar Yadav
111601013

This repositry contains a tiger to mips compiler.

Dev folder tracks chapterwise developemet of the project.

Final folder has the final working compiler.

Testcases are contained in the directory Testcases under Final folder.

Workflow of the complier is captured by two png files 'chapterwise_tasks.png' and 'workflow_diagram.png' stored inside Dev folder

How to run a file - 
    1) Change the permissions of the bash files (compile and execute) by typing chmod 755 compile && chmod 755 execute.
    2) Place the test file in the Final directory.
    3) ./compile <test_filename.tig>
    4) Now a file a.s will be created. Execute it by typing ./execute .

Salient features of the compiler
    1) Compile full tiger language.
    2) Implements proper scoping and also permits declaration functions inside functions.
    3) Recursive types and functions.
    4) Semantic analysis.
    5) Implements type checking.
    6) Implements type inference.
    7) Canonicalization.
    8) Converts tiger to IR (tree language).
    9) Converts tree to MIPS by performing instruction selection.
    10) Control flow analysis.
    11) Data flow analysis.
    12) Liveness analysis.
    13) Register allocation by performing coloring.
    14) Implemented spilling of variables to frame.
    15) Proper error messages.
    16) Library function included with the compiler. Like - 
                                                            1) print                              - print a string.
                                                            2) printi                             - print a integer.
                                                            3) flush                              - flush the standard output buffer.
                                                            4) getchar                            - get character from input buffer.
                                                            5) ord                                - ascii value of first character of string.
                                                            6) chr                                - string from ascii value.
                                                            7) size                               - size of string.
                                                            8) substring                          - substring of string.
                                                            9) concat                             - concatenation of two strings.
                                                            10) not                               - return (i=0).
                                                            11) exit                              - halt the program with the passed int as code.
                                                            12) copy                              - copy a string.
                                                            13) strcmp                            - compare two strings.
                                                            14) initArray                         - array
                                                            15) allocRecord                       - allocate space on stack
    17) Properly commented code for easy read and understanding.

Purpose of each file - 
absyn.sml           - Contains abstract syntax tree for tiger language
assem.sml           - Contains parameterization of assembly language
canon.sml           - Used for canonicalize the tree code.
codgen.sml          - Implements instruction selection using maximum munch algorithm.
color.sml           - File to color temporaries for register allocation.
compile             - Bash file to compile a given tiger program to mips.
env.sml             - File storing the structure of variable and type environment.
errormsg.sml        - Contains some error message for compilation phase.
execute             - Bash to file to run compiled code.
findescape.sml      - File to find whether a variable should escape to frame or not.
flowgraph.sml       - Graph for Liveness analysis.
frame.sml           - File contain MipsFrame.
graph.sml           - Aux file for flowgraph.sml.
liveness.sml        - File to calculate the where a variable is live.
main.sml            - File to call functions in other files.
makegraph.sml       - File making control flow graph.
parsetest.sml       - File to check parser.
prabsyn.sml         - File to print abstract syntax tree of tiger language.
printtree.sml       - File to proint abstract syntax tree of tree language.
regalloc.sml        - File to assign registers to temporaries.
runtime.s           - Library file.
semant.sml          - File for semantic analysis and converting tiger to mips.
sources.cm          - File containing name of all sml files.
symbol.sml          - File providing efficient and convenient lookup for variable declarations, type declarations, functions declaration, etc for their types.
table.sml           - File providing efficient and convenient lookup for variable environment, type environment, etc for their types.
temp.sml            - File containing parameterization of temporaries and labels.
tiger.grm           - Grammar file.
tiger.lex           - Lex file.
translate.sml       - File to translate tiger into tree.
tree.sml            - File containing abstract syntax tree for tree language.
types.sml           - File containing parameterization of types in tiger language.
