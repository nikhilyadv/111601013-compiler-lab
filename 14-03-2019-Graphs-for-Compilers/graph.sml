signature GRAPH =
sig
    type graph
    type node
    
    val nodes: graph -> node list
    val succ: node -> node list
    val pred: node -> node list
    val adj: node -> node list   (* succ+pred *)
    val eq: node*node -> bool

    val newGraph: unit -> graph
    val newNode : graph -> node
    exception GraphEdge
    val mk_edge: {from: node, to: node} -> unit
    val rm_edge: {from: node, to: node} -> unit

    val nodename: node->string  (* for debugging only *)

end

structure Graph :> GRAPH =
struct
  type node' = int

  datatype noderep = NODE of {succ: node' list, pred: node' list}

  val emptyNode = NODE{succ=[],pred=[]}

  val bogusNode = NODE{succ=[~1],pred=[]}

  fun isBogus(NODE{succ= ~1::_,...}) = true
    | isBogus _ = false

  structure A = DynamicArrayFn(struct open Array
				    type elem = noderep
				    type vector = noderep vector
				    type array = noderep array
                             end)

  type graph = A.array

  type node = graph * node'
  fun eq((_,a),(_,b)) = a=b

  fun augment (g: graph) (n: node') : node = (g,n)

  fun newGraph() = A.array(0,bogusNode)

  fun nodes g = let val b = A.bound g
                    fun f i = if isBogus( A.sub(g,i)) then nil
                            else (g,i)::f(i+1)
                in f 0			     
                end

  fun succ(g,i) = let val NODE{succ=s,...} = A.sub(g,i) 
		   in map (augment g) s 
		  end
  fun pred(g,i) = let val NODE{pred=p,...} = A.sub(g,i)
                     in map (augment g) p 
		  end
  fun adj gi = pred gi @ succ gi

  fun newNode g = (* binary search for unused node *)
    let fun look(lo,hi) =
               (* i < lo indicates i in use
                  i >= hi indicates i not in use *)
            if lo=hi then (A.update(g,lo,emptyNode); (g,lo))
            else let val m = (lo+hi) div 2
                  in if isBogus(A.sub(g,m)) then look(lo,m) else look(m+1,hi)
                 end
     in look(0, 1 + A.bound g)
    end

  exception GraphEdge
  fun check(g,g') = (* if g=g' then () else raise GraphEdge *) ()

  fun delete(i,j::rest) = if i=j then rest else j::delete(i,rest)
    | delete(_,nil) = raise GraphEdge

  fun diddle_edge change {from=(g:graph, i),to=(g':graph, j)} = 
      let val _ = check(g,g')
          val NODE{succ=si,pred=pi} = A.sub(g,i)
          val _ = A.update(g,i,NODE{succ=change(j,si),pred=pi})
          val NODE{succ=sj,pred=pj} = A.sub(g,j)
          val _ = A.update(g,j,NODE{succ=sj,pred=change(i,pj)})
       in ()
      end

  val mk_edge = diddle_edge (op ::)
  val rm_edge = diddle_edge delete

  fun nodename(g,i:int) = "n" ^ Int.toString(i)

end


(* (* dynamic-array-fn.sml
 *
 * COPYRIGHT (c) 1993 by AT&T Bell Laboratories.  See COPYRIGHT file for details.
 *
 * Arrays of unbounded length
 *
 *)

functor DynamicArrayFn (A : MONO_ARRAY) : MONO_DYNAMIC_ARRAY =
  struct

    type elem = A.elem
    datatype array = BLOCK of A.array ref * elem * int ref
 
    exception Subscript = General.Subscript
    exception Size = General.Size

    fun array (sz, dflt) = BLOCK(ref (A.array (sz, dflt)), dflt, ref (~1))

  (* fromList (l, v) creates an array using the list of values l
   * plus the default value v.
   * NOTE: Once MONO_ARRAY includes arrayoflist, this will become trivial.
   *)
    fun fromList (initList, dflt) = let
          val len = length initList
	  val arr = A.array(len, dflt)
	  fun upd ([], _) = ()
	    | upd (x::r, i) = (A.update(arr, i, x); upd(r, i+1))
	  in
	    upd (initList, 0);
	    BLOCK(ref arr, dflt, ref (len-1))
	  end

  (* tabulate (sz,fill,dflt) acts like Array.tabulate, plus 
   * stores default value dflt.  Raises Size if sz < 0.
   *)
    fun tabulate (sz, fillFn, dflt) =
	  BLOCK(ref(A.tabulate(sz, fillFn)), dflt, ref (sz-1))

    fun subArray (BLOCK(arr,dflt,bnd),lo,hi) = let
          val arrval = !arr
          val bnd = !bnd
          fun copy i = A.sub(arrval,i+lo)
          in
            if hi <= bnd
              then BLOCK(ref(A.tabulate(hi-lo,copy)), dflt, ref (hi-lo))
            else if lo <= bnd 
              then BLOCK(ref(A.tabulate(bnd-lo,copy)),dflt,ref(bnd-lo))
            else
              array(0,dflt)
          end

    fun default (BLOCK(_,dflt,_)) = dflt

    fun sub (BLOCK(arr,dflt,_),idx) = (A.sub(!arr,idx)) 
          handle Subscript => if idx < 0 then raise Subscript else dflt

    fun bound (BLOCK(_,_,bnd)) = (!bnd)

    fun expand(arr,oldlen,newlen,dflt) = let
          fun fillfn i = if i < oldlen then A.sub(arr,i) else dflt
          in
            A.tabulate(newlen, fillfn)
          end

    fun update (BLOCK(arr,dflt,bnd),idx,v) = let 
          val len = A.length (!arr)
          in
            if idx >= len 
              then arr := expand(!arr,len, Int.max(len+len,idx+1),dflt) 
              else ();
            A.update(!arr,idx,v);
            if idx > !bnd then bnd := idx else ()
          end

    fun truncate (a as BLOCK(arr,dflt,bndref),sz) = let
          val bnd = !bndref
          val newbnd = sz - 1
          val arr_val = !arr
          val array_sz = A.length arr_val
          fun fillDflt (i,stop) =
                if i = stop then ()
                else (A.update(arr_val,i,dflt);fillDflt(i-1,stop))
          in
            if newbnd < 0 then (bndref := ~1;arr := A.array(0,dflt))
            else if newbnd >= bnd then ()
            else if 3 * sz < array_sz then let
              val BLOCK(arr',_,bnd') = subArray(a,0,newbnd)
              in
                (bndref := !bnd'; arr := !arr')
              end
            else fillDflt(bnd,newbnd)
          end

  end (* DynamicArrayFn *)
 *)




(* Interface
eqtype 'a array = 'a array 
type 'a vector = 'a Vector.vector
val maxLen : int
val array : int * 'a -> 'a array
val fromList : 'a list -> 'a array
val tabulate : int * (int -> 'a) -> 'a array
val length : 'a array -> int
val sub : 'a array * int -> 'a
val update : 'a array * int * 'a -> unit
val vector : 'a array -> 'a vector
val copy    : {src : 'a array, dst : 'a array, di : int}
                -> unit
val copyVec : {src : 'a vector, dst : 'a array, di : int}
                -> unit
val appi : (int * 'a -> unit) -> 'a array -> unit
val app  : ('a -> unit) -> 'a array -> unit
val modifyi : (int * 'a -> 'a) -> 'a array -> unit
val modify  : ('a -> 'a) -> 'a array -> unit
val foldli : (int * 'a * 'b -> 'b) -> 'b -> 'a array -> 'b
val foldri : (int * 'a * 'b -> 'b) -> 'b -> 'a array -> 'b
val foldl  : ('a * 'b -> 'b) -> 'b -> 'a array -> 'b
val foldr  : ('a * 'b -> 'b) -> 'b -> 'a array -> 'b
val findi : (int * 'a -> bool)
              -> 'a array -> (int * 'a) option
val find  : ('a -> bool) -> 'a array -> 'a option
val exists : ('a -> bool) -> 'a array -> bool
val all : ('a -> bool) -> 'a array -> bool
val collate : ('a * 'a -> order)
                -> 'a array * 'a array -> order
Description
val maxLen : int
The maximum length of arrays supported by this implementation. Attempts to create larger arrays will result in the Size exception being raised. 

array (n, init)
creates a new array of length n; each element is initialized to the value init. If n < 0 or maxLen < n, then the Size exception is raised. 

fromList l
creates a new array from l. The length of the array is length l and the i(th) element of the array is the i(th) element of the the list. If the length of the list is greater than maxLen, then the Size exception is raised. 

tabulate (n, f)
creates an array of n elements, where the elements are defined in order of increasing index by applying f to the element's index. This is equivalent to the expression:
fromList (List.tabulate (n, f))
If n < 0 or maxLen < n, then the Size exception is raised. 

length arr
returns |arr|, the length of the array arr. 

sub (arr, i)
returns the i(th) element of the array arr. If i < 0 or |arr| <= i, then the Subscript exception is raised. 

update (arr, i, x)
sets the i(th) element of the array arr to x. If i < 0 or |arr| <= i, then the Subscript exception is raised. 

vector arr
generates a vector from arr. Specifically, the result is equivalent to
          Vector.tabulate (length arr, fn i => sub (arr, i))
          


copy {src, dst, di}
copyVec {src, dst, di}
These functions copy the entire array or vector src into the array dst, with the i(th) element in src, for 0 <= i < |src|, being copied to position di + i in the destination array. If di < 0 or if |dst| < di+|src|, then the Subscript exception is raised.
Implementation note:
In copy, if dst and src are equal, we must have di = 0 to avoid an exception, and copy is then the identity.



appi f arr
app f arr
These apply the function f to the elements of the array arr in order of increasing indices. The more general form appi supplies f with the array index of the corresponding element. 

modifyi f arr
modify f arr
These apply the function f to the elements of the array arr in order of increasing indices, and replace each element with the result. The more general modifyi supplies f with the array index of the corresponding element. The expression modify f arr is equivalent to modifyi (f o #2) arr. 

foldli f init arr
foldri f init arr
foldl f init arr
foldr f init arr
These fold the function f over all the elements of the array arr, using the value init as the initial value. The functions foldli and foldl apply the function f from left to right (increasing indices), while the functions foldri and foldr work from right to left (decreasing indices). The more general functions foldli and foldri supply f with the array index of the corresponding element.
Refer to the MONO_ARRAY manual pages for reference implementations of the indexed versions.

The expression foldl f init arr is equivalent to:

foldli (fn (_, a, x) => f(a, x)) init arr
The analogous equivalences hold for foldri and foldr. 

findi f arr
find f arr
These functions apply f to each element of the array arr, from left to right (i.e., increasing indices), until a true value is returned. If this occurs, the functions return the element; otherwise, they return NONE. The more general version findi also supplies f with the array index of the element and, upon finding an entry satisfying the predicate, returns that index with the element. 

exists f arr
applies f to each element x of the array arr, from left to right (i.e., increasing indices), until f x evaluates to true; it returns true if such an x exists and false otherwise. 

all f arr
applies f to each element x of the array arr, from left to right (i.e., increasing indices), until f x evaluates to false; it returns false if such an x exists and true otherwise. It is equivalent to not(exists (not o f) arr)). 

collate f (a1, a2)
performs lexicographic comparison of the two arrays using the given ordering f on elements. 
 *)