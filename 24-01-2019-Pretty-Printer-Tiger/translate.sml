structure Translate =
struct
    val reset_color = "\027[0m"
    val keyword  = "\027[1;31m"
    val identifier = "\027[1;34m"
    val symbol = "\027[1;35m"
    fun prettyprint 0 = ()
      | prettyprint n = (print "\t"; prettyprint (n - 1))
    
    fun compileExpr (Ast.NIL) indentlevel =   print "nil"
      | compileExpr (Ast.Constant(a)) indentlevel =   print (identifier ^ (Int.toString(a)) ^ reset_color)
      | compileExpr (Ast.Op(a,Ast.PLUS,b)) indentlevel = (compileExpr a indentlevel; print (symbol ^ " + " ^ reset_color); compileExpr b indentlevel)
      | compileExpr (Ast.Op(a,Ast.MINUS,b)) indentlevel = (compileExpr a indentlevel; print (symbol ^ " - " ^ reset_color); compileExpr b indentlevel)
      | compileExpr (Ast.Op(a,Ast.MUL,b)) indentlevel = (compileExpr a indentlevel; print (symbol ^ " * " ^ reset_color); compileExpr b indentlevel)
      | compileExpr (Ast.Op(a,Ast.DIVI,b)) indentlevel = (compileExpr a indentlevel; print (symbol ^ " / " ^ reset_color); compileExpr b indentlevel)
      | compileExpr (Ast.IfThen(expr1,expr2)) indentlevel = (print (keyword ^ "if " ^ reset_color); compileExpr expr1 (indentlevel); print (keyword ^ " then\n" ^ reset_color); prettyprint (indentlevel+1); compileExpr expr2 (indentlevel+1); print "\n")
      | compileExpr (Ast.IfThenElse(expr1,expr2,expr3)) indentlevel = (print (keyword ^ "if " ^ reset_color); compileExpr expr1 (indentlevel); print (keyword ^ " then\n" ^ reset_color); prettyprint (indentlevel+1); compileExpr expr2 (indentlevel+1); print "\n"; prettyprint (indentlevel); print (keyword ^ "else\n" ^ reset_color); prettyprint (indentlevel+1); compileExpr expr3 (indentlevel+1); print "\n")

    fun compileExprs [] _ = []
      | compileExprs (x::xs) indentlevel = (compileExpr x indentlevel; compileExprs xs indentlevel)

    fun compile Expressions indentlevel = (*do something here*) compileExprs Expressions indentlevel

end