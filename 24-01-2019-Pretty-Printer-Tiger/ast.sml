structure Ast =
struct

datatype BinOp      =   PLUS
                    |   MINUS
                    |   MUL
                    |   DIVI

datatype ComparisonOp   =   LT
                        |   LE
                        |   EQ
                        |   GE
                        |   GT
                        |   NEQ

datatype Expression =   NIL 
                    |   Constant of int
                    |   Op of Expression * BinOp * Expression
                    |   IfThen  of Expression * Expression 
                    |   IfThenElse  of Expression * Expression * Expression

fun plus a b    =   Op(a, PLUS, b)
fun minus a b   =   Op(a, MINUS, b)
fun mul a b     =   Op(a, MUL, b)
fun divi a b    =   Op(a, DIVI, b)

end
