type lineNo             = int
type pos                = lineNo
val lineRef : pos ref   = ref 0
val colRef  : pos ref   = ref 0

fun updateLine n        = (lineRef := !(lineRef) + n; colRef := 0)
fun updateColumn n      = colRef  := !(colRef)  + n

type svalue             = Tokens.svalue
type ('a,'b) token      = ('a,'b) Tokens.token
type lexresult          = (svalue,pos) token

fun eof()               = Tokens.EOF(!lineRef,!lineRef)

fun charsToInt m (x :: xs) = charsToInt (10 * m + ord x - ord #"0") xs
  | charsToInt m []        = m

fun toSigned (#"-" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"~" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"+" :: xs) =   charsToInt 0 xs
  | toSigned xs           =   charsToInt 0 xs

val toInt               = toSigned o String.explode

fun error ()            = print ("Error at line " ^ Int.toString(!lineRef) ^ " Column " ^ Int.toString(!colRef) ^ "\n");

%%
%header (functor ExprLexFun(structure Tokens : Expr_TOKENS));
digit   = [0-9]+;
%%
"\n"                    =>  (updateLine 1; lex());
" "                     =>  (updateColumn 1; lex());
"\t"                    =>  (updateColumn 4; lex());            
"if"                    =>  (updateColumn 2; Tokens.IF(!lineRef,!colRef));
"then"                  =>  (updateColumn 4; Tokens.THEN(!lineRef,!colRef));
"else"                  =>  (updateColumn 4; Tokens.ELSE(!lineRef,!colRef));
{digit}+                =>  (updateColumn (size(yytext)); Tokens.CONST(toInt yytext, !lineRef, !colRef));
"+"                     =>  (updateColumn 1; Tokens.PLUS(!lineRef,!colRef));
"-"                     =>  (updateColumn 1; Tokens.MINUS(!lineRef,!colRef));
"*"                     =>  (updateColumn 1; Tokens.MUL(!lineRef,!colRef));
"/"                     =>  (updateColumn 1; Tokens.DIVI(!lineRef,!colRef));
"("                     =>  (updateColumn 1; Tokens.LPAREN(!lineRef,!colRef));
")"                     =>  (updateColumn 1; Tokens.RPAREN(!lineRef,!colRef));
.                       =>  (updateColumn (size(yytext)); error (); lex());