functor ExprLrValsFun(structure Token : TOKEN)
 : sig structure ParserData : PARSER_DATA
       structure Tokens : Expr_TOKENS
   end
 = 
struct
structure ParserData=
struct
structure Header = 
struct
(*#line 1.2 "tiger.grm"*)

(*#line 13.1 "tiger.grm.sml"*)
end
structure LrTable = Token.LrTable
structure Token = Token
local open LrTable in 
val table=let val actionRows =
"\
\\001\000\001\000\007\000\006\000\006\000\008\000\005\000\000\000\
\\001\000\002\000\012\000\003\000\011\000\004\000\010\000\005\000\009\000\
\\007\000\020\000\000\000\
\\001\000\002\000\012\000\003\000\011\000\004\000\010\000\005\000\009\000\
\\009\000\019\000\000\000\
\\001\000\011\000\000\000\000\000\
\\025\000\000\000\
\\026\000\001\000\007\000\002\000\012\000\003\000\011\000\004\000\010\000\
\\005\000\009\000\006\000\006\000\008\000\005\000\000\000\
\\026\000\001\000\007\000\006\000\006\000\008\000\005\000\000\000\
\\027\000\000\000\
\\028\000\000\000\
\\029\000\004\000\010\000\005\000\009\000\000\000\
\\030\000\004\000\010\000\005\000\009\000\000\000\
\\031\000\000\000\
\\032\000\000\000\
\\033\000\000\000\
\\034\000\002\000\012\000\003\000\011\000\004\000\010\000\005\000\009\000\
\\010\000\022\000\000\000\
\\035\000\002\000\012\000\003\000\011\000\004\000\010\000\005\000\009\000\000\000\
\"
val actionRowNumbers =
"\006\000\004\000\005\000\000\000\
\\000\000\008\000\007\000\000\000\
\\000\000\000\000\000\000\002\000\
\\001\000\012\000\011\000\010\000\
\\009\000\000\000\013\000\014\000\
\\000\000\015\000\003\000"
val gotoT =
"\
\\001\000\002\000\002\000\001\000\003\000\022\000\000\000\
\\000\000\
\\001\000\002\000\002\000\006\000\000\000\
\\001\000\011\000\000\000\
\\001\000\012\000\000\000\
\\000\000\
\\000\000\
\\001\000\013\000\000\000\
\\001\000\014\000\000\000\
\\001\000\015\000\000\000\
\\001\000\016\000\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\000\000\
\\001\000\019\000\000\000\
\\000\000\
\\000\000\
\\001\000\021\000\000\000\
\\000\000\
\\000\000\
\"
val numstates = 23
val numrules = 11
val s = ref "" and index = ref 0
val string_to_int = fn () => 
let val i = !index
in index := i+2; Char.ord(String.sub(!s,i)) + Char.ord(String.sub(!s,i+1)) * 256
end
val string_to_list = fn s' =>
    let val len = String.size s'
        fun f () =
           if !index < len then string_to_int() :: f()
           else nil
   in index := 0; s := s'; f ()
   end
val string_to_pairlist = fn (conv_key,conv_entry) =>
     let fun f () =
         case string_to_int()
         of 0 => EMPTY
          | n => PAIR(conv_key (n-1),conv_entry (string_to_int()),f())
     in f
     end
val string_to_pairlist_default = fn (conv_key,conv_entry) =>
    let val conv_row = string_to_pairlist(conv_key,conv_entry)
    in fn () =>
       let val default = conv_entry(string_to_int())
           val row = conv_row()
       in (row,default)
       end
   end
val string_to_table = fn (convert_row,s') =>
    let val len = String.size s'
        fun f ()=
           if !index < len then convert_row() :: f()
           else nil
     in (s := s'; index := 0; f ())
     end
local
  val memo = Array.array(numstates+numrules,ERROR)
  val _ =let fun g i=(Array.update(memo,i,REDUCE(i-numstates)); g(i+1))
       fun f i =
            if i=numstates then g i
            else (Array.update(memo,i,SHIFT (STATE i)); f (i+1))
          in f 0 handle Subscript => ()
          end
in
val entry_to_action = fn 0 => ACCEPT | 1 => ERROR | j => Array.sub(memo,(j-2))
end
val gotoT=Array.fromList(string_to_table(string_to_pairlist(NT,STATE),gotoT))
val actionRows=string_to_table(string_to_pairlist_default(T,entry_to_action),actionRows)
val actionRowNumbers = string_to_list actionRowNumbers
val actionT = let val actionRowLookUp=
let val a=Array.fromList(actionRows) in fn i=>Array.sub(a,i) end
in Array.fromList(map actionRowLookUp actionRowNumbers)
end
in LrTable.mkLrTable {actions=actionT,gotos=gotoT,numRules=numrules,
numStates=numstates,initialState=STATE 0}
end
end
local open Header in
type pos = int
type arg = unit
structure MlyValue = 
struct
datatype svalue = VOID | ntVOID of unit | CONST of  (int) | PROGRAM of  (Ast.Expression list) | EXPRS of  (Ast.Expression list) | EXPR of  (Ast.Expression)
end
type svalue = MlyValue.svalue
type result = Ast.Expression list
end
structure EC=
struct
open LrTable
infix 5 $$
fun x $$ y = y::x
val is_keyword =
fn _ => false
val preferred_change : (term list * term list) list = 
nil
val noShift = 
fn (T 10) => true | _ => false
val showTerminal =
fn (T 0) => "CONST"
  | (T 1) => "PLUS"
  | (T 2) => "MINUS"
  | (T 3) => "MUL"
  | (T 4) => "DIVI"
  | (T 5) => "LPAREN"
  | (T 6) => "RPAREN"
  | (T 7) => "IF"
  | (T 8) => "THEN"
  | (T 9) => "ELSE"
  | (T 10) => "EOF"
  | _ => "bogus-term"
local open Header in
val errtermvalue=
fn _ => MlyValue.VOID
end
val terms : term list = nil
 $$ (T 10) $$ (T 9) $$ (T 8) $$ (T 7) $$ (T 6) $$ (T 5) $$ (T 4) $$ (T 3) $$ (T 2) $$ (T 1)end
structure Actions =
struct 
exception mlyAction of int
local open Header in
val actions = 
fn (i392,defaultPos,stack,
    (()):arg) =>
case (i392,stack)
of  ( 0, ( ( _, ( MlyValue.EXPRS EXPRS, EXPRS1left, EXPRS1right)) :: rest671)) => let val  result = MlyValue.PROGRAM ((*#line 34.29 "tiger.grm"*)EXPRS(*#line 180.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 2, ( result, EXPRS1left, EXPRS1right), rest671)
end
|  ( 1, ( rest671)) => let val  result = MlyValue.EXPRS ((*#line 36.34 "tiger.grm"*)[](*#line 184.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, defaultPos, defaultPos), rest671)
end
|  ( 2, ( ( _, ( MlyValue.EXPRS EXPRS, _, EXPRS1right)) :: ( _, ( MlyValue.EXPR EXPR, EXPR1left, _)) :: rest671)) => let val  result = MlyValue.EXPRS ((*#line 37.34 "tiger.grm"*)EXPR::EXPRS(*#line 188.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 1, ( result, EXPR1left, EXPRS1right), rest671)
end
|  ( 3, ( ( _, ( MlyValue.CONST CONST, CONST1left, CONST1right)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 39.38 "tiger.grm"*)Ast.Constant CONST(*#line 192.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, CONST1left, CONST1right), rest671)
end
|  ( 4, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 40.38 "tiger.grm"*)Ast.plus EXPR1 EXPR2(*#line 196.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 5, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 41.38 "tiger.grm"*)Ast.minus EXPR1 EXPR2(*#line 200.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 6, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 42.38 "tiger.grm"*)Ast.mul EXPR1 EXPR2(*#line 204.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 7, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, EXPR1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 43.38 "tiger.grm"*)Ast.divi EXPR1 EXPR2(*#line 208.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, EXPR1left, EXPR2right), rest671)
end
|  ( 8, ( ( _, ( _, _, RPAREN1right)) :: ( _, ( MlyValue.EXPR EXPR, _, _)) :: ( _, ( _, LPAREN1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 44.38 "tiger.grm"*)EXPR(*#line 212.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, LPAREN1left, RPAREN1right), rest671)
end
|  ( 9, ( ( _, ( MlyValue.EXPR EXPR2, _, EXPR2right)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 45.38 "tiger.grm"*)Ast.IfThen(EXPR1,EXPR2)(*#line 216.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, IF1left, EXPR2right), rest671)
end
|  ( 10, ( ( _, ( MlyValue.EXPR EXPR3, _, EXPR3right)) :: _ :: ( _, ( MlyValue.EXPR EXPR2, _, _)) :: _ :: ( _, ( MlyValue.EXPR EXPR1, _, _)) :: ( _, ( _, IF1left, _)) :: rest671)) => let val  result = MlyValue.EXPR ((*#line 46.49 "tiger.grm"*)Ast.IfThenElse(EXPR1,EXPR2,EXPR3)(*#line 220.1 "tiger.grm.sml"*)
)
 in ( LrTable.NT 0, ( result, IF1left, EXPR3right), rest671)
end
| _ => raise (mlyAction i392)
end
val void = MlyValue.VOID
val extract = fn a => (fn MlyValue.PROGRAM x => x
| _ => let exception ParseInternal
	in raise ParseInternal end) a 
end
end
structure Tokens : Expr_TOKENS =
struct
type svalue = ParserData.svalue
type ('a,'b) token = ('a,'b) Token.token
fun CONST (i,p1,p2) = Token.TOKEN (ParserData.LrTable.T 0,(ParserData.MlyValue.CONST i,p1,p2))
fun PLUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 1,(ParserData.MlyValue.VOID,p1,p2))
fun MINUS (p1,p2) = Token.TOKEN (ParserData.LrTable.T 2,(ParserData.MlyValue.VOID,p1,p2))
fun MUL (p1,p2) = Token.TOKEN (ParserData.LrTable.T 3,(ParserData.MlyValue.VOID,p1,p2))
fun DIVI (p1,p2) = Token.TOKEN (ParserData.LrTable.T 4,(ParserData.MlyValue.VOID,p1,p2))
fun LPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 5,(ParserData.MlyValue.VOID,p1,p2))
fun RPAREN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 6,(ParserData.MlyValue.VOID,p1,p2))
fun IF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 7,(ParserData.MlyValue.VOID,p1,p2))
fun THEN (p1,p2) = Token.TOKEN (ParserData.LrTable.T 8,(ParserData.MlyValue.VOID,p1,p2))
fun ELSE (p1,p2) = Token.TOKEN (ParserData.LrTable.T 9,(ParserData.MlyValue.VOID,p1,p2))
fun EOF (p1,p2) = Token.TOKEN (ParserData.LrTable.T 10,(ParserData.MlyValue.VOID,p1,p2))
end
end
