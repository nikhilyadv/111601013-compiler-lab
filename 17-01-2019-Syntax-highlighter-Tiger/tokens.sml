structure Tokens : Tiger_tokens =
struct
	type linenum				= int
	type token					= string
	fun ARRAY(i,j)				= "array"
	fun IF(i,j)					= "if"
	fun THEN(i,j) 				= "then"
	fun ELSE(i,j) 				= "else"
	fun WHILE(i,j) 				= "while"
	fun FOR(i,j)				= "for"
	fun TO(i,j)					= "to"
	fun DO(i,j)					= "do"
	fun LET(i,j) 				= "let"
	fun IN(i,j)					= "in"
	fun END(i,j) 				= "end"
	fun OF(i,j) 				= "of"
	fun BREAK(i,j)				= "break"
	fun NIL(i,j) 				= "nil"
	fun FUNCTION(i,j)			= "function"
	fun VAR(i,j)				= "var"
	fun TYPE(i,j)				= "type"
	fun IMPORT(i,j)				= "import"
	fun PRIMITIVE(i,j)			= "primitive"
	fun CLASS(i,j)				= "class"
	fun EXTENDS(i,j)			= "extends"
	fun METHOD(i,j)				= "method"
	fun NEW(i,j)				= "new"

	fun COMMA(i,j) 				= ","
	fun COLON(i,j) 				= ":"
	fun SEMICOLON(i,j) 			= ";"
	fun RPAREN(i,j) 			= ")"
	fun LPAREN(i,j) 			= "("
	fun RBRACK(i,j) 			= "]"
	fun LBRACK(i,j) 			= "["
	fun RBRACE(i,j) 			= "}"
	fun LBRACE(i,j) 			= "{"
	fun DOT(i,j) 				= "."
	fun PLUS(i,j)				= "+"
	fun MINUS(i,j)				= "-"
	fun TIMES(i,j)				= "*"
	fun DIVIDE(i,j)				= "/"
	fun MOD(i,j)				= "%"
	fun EQ(i,j) 				= "="
	fun NEQ(i,j) 				= "<>"
	fun LT(i,j)					= "<"
	fun LE(i,j) 				= "<="
	fun GT(i,j) 				= ">"
	fun GE(i,j) 				= ">="	
	fun AND(i,j) 				= "&"
	fun OR(i,j)					= "|"
	fun COLONEQ(i,j)			= ":="
	
	fun SPACE(i,j) 				= " "
	fun TAB(i,j) 				= "\t" 
	fun NEWLINE(i,j)			= "\n"	
	
	fun SCOMMENT(i,j)			= "/*"
	fun ECOMMENT(i,j)			= "*/"
	fun COMMENTSTRING(c,i,j)	= "1C" ^ c
	
	fun INT(c,i,j)				= c	(*Int.toString(c)*)
	fun STRING(c,i,j)			= c
	fun ID(c,i,j)				= c	
	
	fun DOUBLEQUOTE(i,j)		= "\""
	
	fun EOF(i,j)				= "EOF"
end
