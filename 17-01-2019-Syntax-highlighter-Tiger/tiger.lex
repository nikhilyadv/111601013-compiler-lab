type lexresult 	= Tokens.token
type lineNo 	= int
type pos 		= lineNo
val lineRef :pos ref = ref 0
val countComment = ref 0

fun eof() 		= let
					val _ =
				  	if (!countComment <> 0)
				  	then print("ERROR:Unbalanced Comments\n")
				  	else ()
				  in
				  	Tokens.EOF(0,0)
				  end


fun updateLine n = lineRef := !(lineRef) + n
fun updateCount n = countComment := !(countComment) + n
%%
%s COMMENT;
digit = [0-9]+;
escape = [abfnrtv] | num | xnum | \" | \\;
%%

<INITIAL>"array" 				=> (Tokens.ARRAY(!lineRef,!lineRef));
<INITIAL>"if"					=> (Tokens.IF(!lineRef,!lineRef));
<INITIAL>"then"					=> (Tokens.THEN(!lineRef,!lineRef));
<INITIAL>"else"					=> (Tokens.ELSE(!lineRef,!lineRef));
<INITIAL>"while"				=> (Tokens.WHILE(!lineRef,!lineRef));
<INITIAL>"for"					=> (Tokens.FOR(!lineRef,!lineRef));
<INITIAL>"to"					=> (Tokens.TO(!lineRef,!lineRef));
<INITIAL>"do"					=> (Tokens.DO(!lineRef,!lineRef));
<INITIAL>"let"					=> (Tokens.LET(!lineRef,!lineRef));
<INITIAL>"in"					=> (Tokens.IN(!lineRef,!lineRef));
<INITIAL>"end"					=> (Tokens.END(!lineRef,!lineRef));
<INITIAL>"of"					=> (Tokens.OF(!lineRef,!lineRef));
<INITIAL>"break"				=> (Tokens.BREAK(!lineRef,!lineRef));
<INITIAL>"nil"					=> (Tokens.NIL(!lineRef,!lineRef));
<INITIAL>"function"				=> (Tokens.FUNCTION(!lineRef,!lineRef));
<INITIAL>"var"					=> (Tokens.VAR(!lineRef,!lineRef));
<INITIAL>"type"					=> (Tokens.TYPE(!lineRef,!lineRef));
<INITIAL>"import"				=> (Tokens.IMPORT(!lineRef,!lineRef));
<INITIAL>"primitive"			=> (Tokens.PRIMITIVE(!lineRef,!lineRef));
<INITIAL>"class"				=> (Tokens.CLASS(!lineRef,!lineRef));
<INITIAL>"extends"				=> (Tokens.EXTENDS(!lineRef,!lineRef));
<INITIAL>"method"				=> (Tokens.METHOD(!lineRef,!lineRef));
<INITIAL>"new"					=> (Tokens.NEW(!lineRef,!lineRef));

<INITIAL>","					=> (Tokens.COMMA(!lineRef,!lineRef));
<INITIAL>":"					=> (Tokens.COLON(!lineRef,!lineRef));
<INITIAL>"\;"					=> (Tokens.SEMICOLON(!lineRef,!lineRef));
<INITIAL>"\)"					=> (Tokens.RPAREN(!lineRef,!lineRef));
<INITIAL>"\("					=> (Tokens.LPAREN(!lineRef,!lineRef));
<INITIAL>"]"					=> (Tokens.RBRACK(!lineRef,!lineRef));
<INITIAL>"\["					=> (Tokens.LBRACK(!lineRef,!lineRef));
<INITIAL>"}"					=> (Tokens.RBRACE(!lineRef,!lineRef));
<INITIAL>"\{"					=> (Tokens.LBRACE(!lineRef,!lineRef));
<INITIAL>"\."					=> (Tokens.DOT(!lineRef,!lineRef));
<INITIAL>"\+"					=> (Tokens.PLUS(!lineRef,!lineRef));
<INITIAL>"-"					=> (Tokens.MINUS(!lineRef,!lineRef));
<INITIAL>"\*"					=> (Tokens.TIMES(!lineRef,!lineRef));
<INITIAL>"\/"					=> (Tokens.DIVIDE(!lineRef,!lineRef));
<INITIAL>"%"					=> (Tokens.MOD(!lineRef,!lineRef));
<INITIAL>"\="					=> (Tokens.EQ(!lineRef,!lineRef));
<INITIAL>"\<\>"					=> (Tokens.NEQ(!lineRef,!lineRef));
<INITIAL>"<"					=> (Tokens.LT(!lineRef,!lineRef));
<INITIAL>"<="					=> (Tokens.LE(!lineRef,!lineRef));
<INITIAL>">"					=> (Tokens.GT(!lineRef,!lineRef));
<INITIAL>">="					=> (Tokens.GE(!lineRef,!lineRef));
<INITIAL>"&"					=> (Tokens.AND(!lineRef,!lineRef));
<INITIAL>"\|"					=> (Tokens.OR(!lineRef,!lineRef));
<INITIAL>":="					=> (Tokens.COLONEQ(!lineRef,!lineRef));

<INITIAL>"\""([^"\\"]|\\{escape})*"\""		=> (Tokens.STRING(yytext,!lineRef,!lineRef));

<INITIAL>" "				   	=> (Tokens.SPACE(!lineRef,!lineRef));
<INITIAL>\t						=> (Tokens.TAB(!lineRef,!lineRef));
<INITIAL>\n						=> (Tokens.NEWLINE(!lineRef,!lineRef));

<INITIAL>"/*"					=> (updateCount 1; YYBEGIN COMMENT; Tokens.SCOMMENT(!lineRef,!lineRef));
<COMMENT>"/*"					=> (updateCount 1; Tokens.SCOMMENT(!lineRef,!lineRef));
<COMMENT>"*/"					=> (updateCount ~1; if (!countComment)=0 then YYBEGIN INITIAL else YYBEGIN COMMENT; Tokens.ECOMMENT(!lineRef,!lineRef));
<COMMENT>\n						=> (Tokens.NEWLINE(!lineRef,!lineRef));
<COMMENT>\t						=> (Tokens.TAB(!lineRef,!lineRef));
<COMMENT>.						=> (Tokens.COMMENTSTRING(yytext,!lineRef,!lineRef));

<INITIAL>{digit}+				=> (Tokens.INT(yytext,!lineRef,!lineRef));
<INITIAL>[a-zA-z][a-zA-Z0-9_]*	=> (Tokens.ID(yytext,!lineRef,!lineRef));
<INITIAL>"_main"				=> (Tokens.ID(yytext,!lineRef,!lineRef));
<INITIAL>.						=> (print "ERROR" ; lex());
