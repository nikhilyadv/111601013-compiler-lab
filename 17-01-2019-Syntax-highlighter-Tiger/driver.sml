structure Parse =
struct 
	fun parse filename =
		let val file = TextIO.openIn filename
			fun get _ = TextIO.input file
			val lexer = Mlex.makeLexer get
			fun do_it() =
				let val t = lexer()
				in
					if t="EOF" 
					then ()
					else
						if t="array" orelse t="if" orelse t="then" orelse t="else" orelse t="while" orelse t="for" orelse t="to" orelse t="do" orelse t="let" orelse t="in" orelse t="end" orelse t="of" orelse t="break" orelse t="nil" orelse t="function" orelse t="var" orelse t="type" orelse t="import" orelse t="primitive" orelse t="class" orelse t="extends" orelse t="method" orelse t="new"			(*KEYWORDS*)
					then (print ("\027[1;31m" ^ t ^ "\027[0m"); do_it())
					else		 
						if t="," orelse t=":" orelse t=";" orelse t=")" orelse t="(" orelse t="]" orelse t="[" orelse t="}" orelse t="{" orelse t="." orelse t="+" orelse t="-" orelse t="*" orelse t="/" orelse t="%" orelse t="=" orelse t="<>" orelse t="<" orelse t="<=" orelse t=">" orelse t=">=" orelse t="&" orelse t="|" orelse t=":="																				(*SYMBOLS*)
						then (print ("\027[1;35m" ^ t ^ "\027[0m"); do_it())
			   			else
			   				if t="/*" orelse t="*/"																																				(*COMMENT*)
			   				then (print ("\027[1;32m" ^ t ^ "\027[0m"); do_it())
			   				else
			   					if String.size(t)>2 andalso substring(t,0,2)="1C"																												(*COMMENT*)
			  					then (print ("\027[1;32m" ^ substring(t,2,size(t)-2) ^ "\027[0m"); do_it())
			  					else
			   						if String.size(t)>2 andalso substring(t,0,1)="\""																										(*STRING*)
			   						then (print ("\027[1;36m" ^ t ^ "\027[0m"); do_it())
			   						else
			   							if substring(t,0,1)<>"0" andalso substring(t,0,1)<>"1" andalso substring(t,0,1)<>"2" andalso substring(t,0,1)<>"3" andalso substring(t,0,1)<>"4" andalso substring(t,0,1)<>"5" andalso substring(t,0,1)<>"6" andalso substring(t,0,1)<>"7" andalso substring(t,0,1)<>"8" andalso substring(t,0,1)<>"9"																(*IDENTIFIER*)
			   							then (print ("\027[1;34m" ^ t ^ "\027[0m"); do_it())
			   							else																																					(*INTEGER*)
			   								(print ("\027[1;33m" ^ t ^ "\027[0m"); do_it())
			   	end
		in 
			do_it();
			TextIO.closeIn file
		end
end
