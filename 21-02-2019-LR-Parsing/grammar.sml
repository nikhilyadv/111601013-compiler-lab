signature ORD_KEY =
sig
    type ord_key
    val compare : ord_key * ord_key -> order
end

structure Grammar = 
struct
    type RHS = Atom.atom list

    structure RHS_KEY : ORD_KEY =
    struct
        type ord_key = Atom.atom list
        fun compare ([],[]) = EQUAL
        | compare ([],_)  = LESS
        | compare (_,[])  = GREATER
        | compare ((x::xs),(y::ys)) = case Atom.compare (x,y) of
                                        EQUAL => compare (xs,ys)
                                    | LESS  => LESS
                                    | GREATER => GREATER
    end

    structure RHSSet = RedBlackSetFn (RHS_KEY)

    type Productions = RHSSet.set

    type Rules = Productions AtomMap.map

    type Grammar = {symbols : AtomSet.set, tokens : AtomSet.set, rules : Rules}

(*
    val S_ = Atom.atom "S'"
    val S = Atom.atom "S"
    val A = Atom.atom "A"
    val B = Atom.atom "B"

    val a = Atom.atom "a"
    val b = Atom.atom "b"
    val epsilon = Atom.atom "#"
    val dot = Atom.atom "."
    val dollar = Atom.atom "$"
    val nill = Atom.atom "nil"

    val symbols = AtomSet.fromList ([S,A,B])
    val tokens = AtomSet.fromList ([a,b])

    val Sp  = RHSSet.fromList ([[A,a],[B]])
    val Ap  = RHSSet.fromList ([[a]])
    val Bp  = RHSSet.fromList ([[b]])

    val u = AtomMap.insert (AtomMap.empty, S, Sp)
    val u = AtomMap.insert (u, A, Ap);
    val u = AtomMap.insert (u, B, Bp);
*)
    val x = Atom.atom "x"
    val comma = Atom.atom ","
    val lpar = Atom.atom "("
    val rpar = Atom.atom ")"
    val dot = Atom.atom "."
    val dollar = Atom.atom "$"
    val nill = Atom.atom "nil"
    val epsilon = Atom.atom "#"

    val S_ = Atom.atom "S'"
    val S = Atom.atom "S"
    val L = Atom.atom "L"

    val symbols = AtomSet.fromList ([S,L])
    val tokens = AtomSet.fromList ([x,comma,lpar,rpar,dollar])

    val Sp  = RHSSet.fromList ([[lpar,L,rpar],[x]])
    val Lp  = RHSSet.fromList ([[S],[L,comma,S]])

    val u = AtomMap.insert (AtomMap.empty, S, Sp)
    val u = AtomMap.insert (u, L, Lp);
 
    val grammar : Grammar = {symbols = symbols, tokens = tokens, rules = u};
end