structure slr = 
struct
    val State = ref AtomMap.empty
    val index = ref 0
    fun solve grm:Grammar.Grammar = 
      let
        fun closure (i,num) =
            let
            val I = ref Grammar.RHSSet.empty
            val _ = (I := Grammar.RHSSet.union(!I, i))
            fun getdot [] = Grammar.nill
                | getdot [x] = Grammar.nill
                | getdot (x::xs) = if Atom.same (x,Grammar.dot) then
                                    (List.hd xs) 
                                else getdot xs
            fun getrhs [] = []
              | getrhs (x::xs) = xs
            fun cc [] = ()
              | cc (x::xs) =
                  let
                    val nonterminal = (List.hd x)
                    val rhs = (getrhs x)
                    val X = getdot rhs
                    fun addprods (X,[]) = ()
                      | addprods (X,(y::ys)) =
                          let
                            val addme = ([X,Grammar.dot] @ y)
                            val _ = (I := (Grammar.RHSSet.add(!I, addme)))
                          in
                            addprods (X,ys)
                          end
                    val _ = if (AtomSet.member((#symbols grm), X)) then
                             (* add prods x -> .y *)
                             addprods (X,(Grammar.RHSSet.listItems (AtomMap.lookup((#rules grm),X))))
                            else    ()
                  in
                    cc xs
                  end
            val Ilist = Grammar.RHSSet.listItems (!I)
            fun breakprod [] = ()
                | breakprod (x::xs) = (cc x; breakprod xs)
            val _ = cc Ilist
          in
            (if num = 0 then !I else closure (!I,num-1))
          end
        fun goto (I,X) = 
          let
            val j = ref Grammar.RHSSet.empty
            fun getnothead [] = []
              | getnothead (x::xs) = xs
            fun getdotandcheckX (b,[]) = false
              | getdotandcheckX (b,[x]) = false
              | getdotandcheckX (b,x::xs) = if Atom.same(x,Grammar.dot) then
                                              (if (Atom.same(List.hd xs, X)) then
                                                (j := Grammar.RHSSet.add(!j, b @ [X,Grammar.dot] @ (getnothead xs)); true)
                                              else
                                                false)
                                            else  getdotandcheckX (b @ [x],xs)
            fun gg [] = ()
              | gg (x::xs) = 
                  let
                    val _ = getdotandcheckX ([],x) 
                  in
                    gg xs
                  end
            val Ilist = Grammar.RHSSet.listItems (I)
            fun breakprod [] = ()
                | breakprod (x::xs) = (gg x; breakprod xs)
            val _ = gg Ilist
          in
            closure (!j,100)
          end
        fun main () =
          let
            val _ = firstfollow.foo grm
            val T = ref AtomMap.empty
            val E = ref (AtomMap.empty : AtomSet.set AtomMap.map)
            val index = ref 0
            fun addtoT state = 
                let
                  val a = Atom.atom "a"
                  val whichstate = ref a
                  fun chk [] = false
                    | chk (x::xs) = 
                      let
                        val (key,set) = x;
                        val ret = if (Grammar.RHSSet.equal(state,set) = true) then
                                    (whichstate := key; true)
                                  else
                                    chk xs
                      in
                        ret
                      end
                  val prods = AtomMap.listItemsi (!T)
                  val exists = chk prods
                  val ret = if exists = true then
                              (!whichstate)
                            else
                              (index := !index + 1; T := AtomMap.insert(!T, Atom.atom (Int.toString(!index)), state); Atom.atom (Int.toString(!index)))
                in
                  ret
                end
            fun printtransitions ind =
              let
                val _ = print ("############### Transitions ###############\n")
                val trans = AtomMap.find(!E, ind)
                fun pp (NONE) = AtomSet.empty 
                  | pp (SOME x) = x
                fun printtrans [] = ()
                  | printtrans (x::xs) = (print (Atom.toString(x) ^ "\n"); printtrans xs)
              in
                printtrans (AtomSet.listItems (pp trans))
              end
            fun printreductions ind =
              let
                val _ = print ("############### REDUCTIONS ###############\n")
                val state = AtomMap.lookup(!T, ind)
                fun checkreduction [] = false
                  | checkreduction [x] = if Atom.same(x,Grammar.dot) then
                                            true
                                         else false
                  | checkreduction (x::xs) = if Atom.same(x,Grammar.dot) then
                                                false
                                             else checkreduction xs
                fun breakprods [] = ()
                  | breakprods (x::xs) = 
                    let
                      fun printfollow a =
                        let
                          fun goforit () = 
                            let
                              val b = AtomMap.lookup (!firstfollow.follow, a)
                              val c = AtomSet.listItems b
                              fun pp [] = (print "\n")
                                | pp (x::xs) = (print (Atom.toString(x) ^ " "); pp xs)
                            in
                              pp c
                            end
                        in
                          goforit ()
                        end
                      val _ = if (checkreduction x) = true then
                                (if (Atom.same((List.hd x), Grammar.S_)) then
                                    print "Accept it\n"
                                 else
                                (print ("Reduce "^ (Atom.toString(List.hd x)) ^" on ");printfollow (List.hd x) ;()))
                              else  ()
                    in
                      breakprods xs
                    end
                val prods = Grammar.RHSSet.listItems state
              in
                (breakprods prods)
              end
            fun printstate s =
              let
                val (ind,state) = s
                val l = Grammar.RHSSet.listItems state
                fun atomtoString x = Atom.toString(x)
                val _ = print ("----------------State - " ^ (atomtoString ind) ^ "----------------\n")
                fun pppp [] = ()
                  | pppp (x::xs) = (print (atomtoString x); pppp xs)
                fun ppp [] = ()
                  | ppp (x::xs) = (print (atomtoString x); print " -> "; pppp xs) 
                fun pp [] = ()
                  | pp (x::xs) = (ppp x; print "\n"; pp xs)
                fun doit () = (pp l; printtransitions ind; printreductions ind; ())
              in
                doit ()
              end
            val start = ref Grammar.RHSSet.empty
            val _ = (start := Grammar.RHSSet.fromList ([[Grammar.S_,Grammar.dot,Grammar.S,Grammar.dollar]]))
            val _ = (start := closure(!start,100))
            val _ = (addtoT (!start))
            fun printE () =
              let
                val e = []
                fun pe [] = ()
                  | pe (x::xs) = (print (Atom.toString(x) ^ "\n"); pe xs)
              in
                pe (e)
              end
            fun printS () = 
              let
                val s = AtomMap.listItemsi (!T)
                fun split [] = ()
                  | split (x::xs) = (printstate x; split xs)
              in
                split s
              end
            fun repeat (num) =
              let
                val states = AtomMap.listItemsi (!T)
                fun foreachstate [] = ()
                  | foreachstate (x::xs) = 
                    let
                      val (statei,rhset) = x
                      fun foreachitem [] = ()
                        | foreachitem (y::ys) = 
                            let
                              fun go X =
                                let
                                  val j = goto(rhset,X)
                                  val statej = addtoT j
                                  fun getaction q = if AtomSet.member((#symbols grm), q) then
                                                        " goto "
                                                    else 
                                                        " shift to "
                                  fun makeatom (q,r) = Atom.atom (("on " ^ Atom.toString(q) ^ (getaction (q)) ^ Atom.toString(r)))
                                  val curritem = (makeatom (X,statej))
                                  val olditem = (AtomMap.find(!E, statei))
                                  fun getnewitem (NONE) = AtomSet.singleton (curritem)
                                    | getnewitem (SOME (x)) = AtomSet.add(x, curritem)
                                  val _ = (E := AtomMap.insert(!E, statei, getnewitem (olditem)))
                                in
                                  ()
                                end
                              fun finddot [] = ()
                                | finddot [z] = ()
                                | finddot (z::zs) = if Atom.same(z,Grammar.dot) then
                                                      (go (List.hd zs); ())
                                                    else
                                                      finddot zs
                              val xx = finddot y
                            in
                              foreachitem ys
                            end
                      val prods = Grammar.RHSSet.listItems (rhset)
                      val _ = foreachitem prods
                    in
                      foreachstate xs
                    end 
                val _ = foreachstate states
              in
                if num = 0 then
                  ()
                else
                 repeat (num-1)
              end
          in
            (repeat (10); printE(); printS(); ())
          end
      in
        (main (); grm)
      end
end
(* slr.solve (Grammar.grammar); *)