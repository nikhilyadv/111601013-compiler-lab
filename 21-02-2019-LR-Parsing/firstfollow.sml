structure firstfollow =
struct
    val first = ref (AtomMap.empty : AtomSet.set AtomMap.map);
    val nullable = ref (AtomMap.empty : bool AtomMap.map);
    val follow = ref (AtomMap.empty : AtomSet.set AtomMap.map);

    fun isNullable [] = true
      | isNullable (x::xs) = AtomMap.lookup (!nullable, x) andalso isNullable xs

    fun foo (grm : Grammar.Grammar) = let
                            fun addtokenstofirst [] = ()
                                | addtokenstofirst (x::xs) = (first := AtomMap.insert(!first, x, AtomSet.singleton x); addtokenstofirst xs)
                            fun initfirst [] = ()
                                | initfirst (x::xs) = (first := AtomMap.insert(!first, x, AtomSet.empty); initfirst xs)
                            fun initnullable [] = ()
                                | initnullable (x::xs) = (nullable := AtomMap.insert(!nullable, x, false); initnullable xs)
                            fun initfollow [] = () 
                                | initfollow (x::xs) = (follow := AtomMap.insert(!follow, x, AtomSet.empty); initfollow xs)
                            
                            val _ = addtokenstofirst (AtomSet.listItems (#tokens grm))
                            val _ = initfirst (AtomSet.listItems (#symbols grm))
                            val _ = initnullable (AtomSet.listItems (#symbols grm) @ AtomSet.listItems (#tokens grm))
                            val _ = initfollow   (AtomSet.listItems (#symbols grm) @ AtomSet.listItems (#tokens grm))

                            val _ = nullable := AtomMap.insert(!nullable , Grammar.epsilon, true)
                            val prods = AtomMap.listItemsi (#rules grm)

                            fun Nullability [] = true
                                | Nullability (x::xs) = AtomMap.lookup (!nullable, x) andalso Nullability xs

                            fun PrintFollow () = let
                                                        val _ = print "********************Follow******************\n"
                                                        val array = AtomMap.listItemsi (!follow)
                                                        fun ele [] = ()
                                                        | ele (x::xs) = (print ((Atom.toString x) ^ " "); ele xs)
                                                        fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = ele (AtomSet.listItems (set))
                                                                            val _ = print "\n"
                                                                            in
                                                                            incr (xs)
                                                                            end
                                                    in
                                                        incr array
                                                    end

                            fun PrintFirst () = let
                                                        val _ = print "********************First******************\n"
                                                        val array = AtomMap.listItemsi (!first)
                                                        fun ele [] = ()
                                                        | ele (x::xs) = (print ((Atom.toString x) ^ " "); ele xs)
                                                        fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = ele (AtomSet.listItems (set))
                                                                            val _ = print "\n"
                                                                            in
                                                                            incr (xs)
                                                                            end
                                                    in
                                                        incr array
                                                    end

                            fun PrintNullable () = let
                                                        val _ = print "********************Nullable******************\n"
                                                        val array = AtomMap.listItemsi (!nullable)
                                                        fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = if (set = true) then
                                                                                        print "True"
                                                                                    else
                                                                                        print "False"
                                                                            val _ = print "\n"
                                                                            in
                                                                            incr (xs)
                                                                            end
                                                    in
                                                        incr array
                                                    end

                            fun goo pds = let 
                                            fun lookup x y = AtomMap.lookup (!x, y)
                                            fun geteverythingotherthanfirst [] = []
                                            | geteverythingotherthanfirst (x::xs) = xs

                                            val (key, atomlist) = pds
                                            val _ = if (Nullability (atomlist) = true) then 
                                                    nullable := AtomMap.insert(!nullable, key, true)
                                                    else
                                                    ()
                                            val k = length (atomlist)
                                            fun ioo (j, x::xs, []) = ()
                                            | ioo (j, x::xs, y::ys) = let
                                                                            val _ = if (Nullability (x::xs) = true) then
                                                                                    follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup first y))
                                                                                    else ()
                                                                        in
                                                                        ioo (j, x::xs @ [y], ys)
                                                                        end 
                                            (*for each i from 1 to k, each j from i + 1 to k*)
                                            fun hoo (i,[]) = ()
                                            | hoo (i,j::js) = let
                                                                val _ = if (Nullability i = true) then
                                                                            first := AtomMap.insert(!first, key, AtomSet.union (lookup first key, lookup first j))
                                                                        else ()
                                                                val _ = if (length js = 0) then
                                                                            follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup follow key))
                                                                        else
                                                                            if (Nullability js = true) then
                                                                            follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup follow key))
                                                                            else ()
                                                                val _ = if (length js > 0) then
                                                                            (follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup first (hd js))); ioo (j, [hd (js)], (geteverythingotherthanfirst js)))
                                                                        else ()

                                                                in
                                                                hoo (i@[j], js)
                                                                end

                                        in
                                            hoo ([],atomlist)
                                        end
                            fun joo [] = ()
                            | joo (x::xs) = let
                                                val (key, map) = x
                                                val listofatomlist = Grammar.RHSSet.listItems map
                                                fun koo [] = ()
                                                | koo (y::ys) = (goo (key, y); koo ys)
                                                val _ = koo listofatomlist
                                            in
                                                joo xs
                                            end
                            fun iter x = if x > 0 then 
                                            (joo prods; iter (x-1))
                                        else  ()
                            fun prettyprintlist [] = "" 
                            | prettyprintlist (x::xs) = ((Atom.toString x) ^ prettyprintlist xs)
                            fun prettyprint x y z = if Atom.compare (y,Grammar.epsilon) = EQUAL then
                                                    ()
                                                    else
                                                    (print ((Atom.toString x) ^ " on " ^ (Atom.toString y) ^ " does " ^ (Atom.toString x) ^ " ---> " ^ (prettyprintlist z) ^ "\n"); ())
                            fun printit [] = print "List empty\n"
                            | printit (x::xs) = (print ((Atom.toString x) ^ " "); printit xs)
                            fun checkthisprod pds = let
                                                    val (key, atomlist) = pds
                                                    val firstofatomset = AtomMap.lookup (!first, (hd atomlist))
                                                    val firstofatomlist = AtomSet.listItems firstofatomset
                                                    fun look [] = ()
                                                        | look (x::xs) = (prettyprint key x atomlist; look xs)
                                                    val _ = look firstofatomlist
                                                    val followofatomset = AtomMap.lookup (!follow, key)
                                                    val followofatomlist = AtomSet.listItems followofatomset
                                                    val _ = if Nullability atomlist = true then 
                                                                (look followofatomlist; ())
                                                            else ()
                                                    in
                                                    ()
                                                    end
                            fun breakprod [] = ()
                            | breakprod (x::xs) = let
                                                    val (key, map) = x
                                                    val listofatomlist = Grammar.RHSSet.listItems map
                                                    fun listofprod [] = ()
                                                        | listofprod (x::xs) = (checkthisprod (key, x); listofprod xs)
                                                    val _ = listofprod listofatomlist
                                                    in
                                                    breakprod xs
                                                    end
                            fun ll () = let
                                        val _ = print "********************LL 1 List******************\n"
                                        val _ = breakprod prods
                                        in
                                        ()
                                        end
                            in
                            (iter (500); PrintNullable (); PrintFirst (); PrintFollow (); ll (); ())
                            end
end