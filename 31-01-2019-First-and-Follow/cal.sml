type RHS = Atom.atom list

signature ORD_KEY =
sig
  type ord_key
  val compare : ord_key * ord_key -> order
end

structure RHS_KEY : ORD_KEY =
struct
    type ord_key = Atom.atom list
    fun compare ([],[]) = EQUAL
      | compare ([],_)  = LESS
      | compare (_,[])  = GREATER
      | compare ((x::xs),(y::ys)) = case Atom.compare (x,y) of
                                    EQUAL => compare (xs,ys)
                                  | LESS  => LESS
                                  | GREATER => GREATER
end

structure RHSSet = RedBlackSetFn (RHS_KEY)

type Productions = RHSSet.set

type Rules = Productions AtomMap.map

type Grammar = {symbols : AtomSet.set, tokens : AtomSet.set, rules : Rules}


val S = Atom.atom "S"
val A = Atom.atom "A"
val B = Atom.atom "B"

val a = Atom.atom "a"
val b = Atom.atom "b"
val epsilon = Atom.atom "#"

val symbols = AtomSet.fromList ([S,A,B])
val tokens = AtomSet.fromList ([a,b])

val Sp  = RHSSet.fromList ([[A,a],[B]])
val Ap  = RHSSet.fromList ([[a]])
val Bp  = RHSSet.fromList ([[b]])

val u = AtomMap.insert (AtomMap.empty, S, Sp)
val u = AtomMap.insert (u, A, Ap);
val u = AtomMap.insert (u, B, Bp);

(*
val S_ = Atom.atom "S'"
val S = Atom.atom "S"
val E = Atom.atom "E"
val E_ = Atom.atom "E'"
val T = Atom.atom "T"
val T_ = Atom.atom "T'"

val plus = Atom.atom "+"
val mul = Atom.atom "*"
val N = Atom.atom "N"
val dollar = Atom.atom "$"
val epsilon = Atom.atom "#"

val symbols = AtomSet.fromList ([S_,S,E_,E,T_,T])
val tokens = AtomSet.fromList ([plus,mul,N,dollar,epsilon])

val S_p  = RHSSet.fromList ([[S,dollar]])
val Sp = RHSSet.fromList ([[E]])
val E_p = RHSSet.fromList ([[plus,T,E_],[epsilon]])
val Ep = RHSSet.fromList ([[T,E_]])
val T_p = RHSSet.fromList ([[mul,N,T_],[epsilon]])
val Tp = RHSSet.fromList ([[N,T_]])

val u = AtomMap.insert (AtomMap.empty, S_, S_p)
val u = AtomMap.insert (u, S, Sp);
val u = AtomMap.insert (u, E_, E_p);
val u = AtomMap.insert (u, E, Ep);
val u = AtomMap.insert (u, T_, T_p);
val u = AtomMap.insert (u, T, Tp);
*)

val grammar : Grammar = {symbols = symbols, tokens = tokens, rules = u}

fun foo (grm : Grammar) = let
                            val first = ref AtomMap.empty;
                            val nullable = ref AtomMap.empty;
                            val follow = ref AtomMap.empty;

                            fun addtokenstofirst [] = ()
                              | addtokenstofirst (x::xs) = (first := AtomMap.insert(!first, x, AtomSet.singleton x); addtokenstofirst xs)
                            fun initfirst [] = ()
                              | initfirst (x::xs) = (first := AtomMap.insert(!first, x, AtomSet.empty); initfirst xs)
                            fun initnullable [] = ()
                              | initnullable (x::xs) = (nullable := AtomMap.insert(!nullable, x, false); initnullable xs)
                            fun initfollow [] = () 
                              | initfollow (x::xs) = (follow := AtomMap.insert(!follow, x, AtomSet.empty); initfollow xs)
                            
                            val _ = addtokenstofirst (AtomSet.listItems (#tokens grm))
                            val _ = initfirst (AtomSet.listItems (#symbols grm))
                            val _ = initnullable (AtomSet.listItems (#symbols grm) @ AtomSet.listItems (#tokens grm))
                            val _ = initfollow   (AtomSet.listItems (#symbols grm) @ AtomSet.listItems (#tokens grm))

                            val _ = nullable := AtomMap.insert(!nullable , epsilon, true)
                            val prods = AtomMap.listItemsi (#rules grm)

                            fun Nullability [] = true
                              | Nullability (x::xs) = AtomMap.lookup (!nullable, x) andalso Nullability xs

                            fun PrintFollow () = let
                                                      val _ = print "********************Follow******************\n"
                                                      val array = AtomMap.listItemsi (!follow)
                                                      fun ele [] = ()
                                                        | ele (x::xs) = (print ((Atom.toString x) ^ " "); ele xs)
                                                      fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = ele (AtomSet.listItems (set))
                                                                            val _ = print "\n"
                                                                          in
                                                                            incr (xs)
                                                                          end
                                                    in
                                                      incr array
                                                    end

                           fun PrintFirst () = let
                                                      val _ = print "********************First******************\n"
                                                      val array = AtomMap.listItemsi (!first)
                                                      fun ele [] = ()
                                                        | ele (x::xs) = (print ((Atom.toString x) ^ " "); ele xs)
                                                      fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = ele (AtomSet.listItems (set))
                                                                            val _ = print "\n"
                                                                          in
                                                                            incr (xs)
                                                                          end
                                                    in
                                                      incr array
                                                    end

                           fun PrintNullable () = let
                                                      val _ = print "********************Nullable******************\n"
                                                      val array = AtomMap.listItemsi (!nullable)
                                                      fun incr [] = ()
                                                        | incr (x::xs) = let
                                                                            val (key, set) = x
                                                                            val _ = print ((Atom.toString key) ^ " :\t")
                                                                            val _ = if (set = true) then
                                                                                      print "True"
                                                                                    else
                                                                                      print "False"
                                                                            val _ = print "\n"
                                                                          in
                                                                            incr (xs)
                                                                          end
                                                    in
                                                      incr array
                                                    end

                            fun goo pds = let 
                                          fun lookup x y = AtomMap.lookup (!x, y)
                                          fun geteverythingotherthanfirst [] = []
                                            | geteverythingotherthanfirst (x::xs) = xs

                                          val (key, atomlist) = pds
                                          val _ = if (Nullability (atomlist) = true) then 
                                                    nullable := AtomMap.insert(!nullable, key, true)
                                                  else
                                                    ()
                                          val k = length (atomlist)
                                          fun ioo (j, x::xs, []) = ()
                                            | ioo (j, x::xs, y::ys) = let
                                                                          val _ = if (Nullability (x::xs) = true) then
                                                                                    follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup first y))
                                                                                  else ()
                                                                      in
                                                                        ioo (j, x::xs @ [y], ys)
                                                                      end 
                                          (*for each i from 1 to k, each j from i + 1 to k*)
                                          fun hoo (i,[]) = ()
                                            | hoo (i,j::js) = let
                                                                val _ = if (Nullability i = true) then
                                                                          first := AtomMap.insert(!first, key, AtomSet.union (lookup first key, lookup first j))
                                                                        else ()
                                                                val _ = if (length js = 0) then
                                                                          follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup follow key))
                                                                        else
                                                                          if (Nullability js = true) then
                                                                            follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup follow key))
                                                                          else ()
                                                                val _ = if (length js > 0) then
                                                                          (follow := AtomMap.insert(!follow, j, AtomSet.union (lookup follow j, lookup first (hd js))); ioo (j, [hd (js)], (geteverythingotherthanfirst js)))
                                                                        else ()

                                                              in
                                                                hoo (i@[j], js)
                                                              end

                                        in
                                          hoo ([],atomlist)
                                        end
                          fun joo [] = ()
                            | joo (x::xs) = let
                                              val (key, map) = x
                                              val listofatomlist = RHSSet.listItems map
                                              fun koo [] = ()
                                                | koo (y::ys) = (goo (key, y); koo ys)
                                              val _ = koo listofatomlist
                                            in
                                              joo xs
                                            end
                            fun iter x = if x > 0 then 
                                            (joo prods; iter (x-1))
                                          else  ()
                          in
                            (iter (500); PrintNullable (); PrintFirst (); PrintFollow(); ())
                          end

val _ = foo grammar;
